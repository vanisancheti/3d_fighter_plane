### Graphics Assignment 2


### Instructions To Compile
>- mkdir build
>- cd build
>- cmake ..
>- make 
>- ./graphics_asgn1

### Instructions to play the game


* Controls ::  
>- UP Key for forward motion with a constant velocity
>- DOWN Key for backward motion with a constant velocity
>- Q Key for rotating anticlockwise
>- E Key for rotating clockwise
>- A Key for tilting leftward
>- D Key for tilting rightward

>- SPACEBAR for increasing the planes height
>- S Key for decreasing the planes height
>- W Key for increasing the speed temporarily
>- V for change the Views
>- * Initially the game will start in the plane's view
>- * Press 'V' once for top view 
>- * Press 'V' again for tower view
>- * Press 'V' again for follow-cam view
>- * Press 'V' again for helicopter view
>- * In helicopter view particular move the mouse to view the plane from different directions
>- * Scroll for zooming in and out


### DASHBOARD
Dashboard has three meters and a display that is implemented through seven segment
>- First meter shows the fuel of the plane
>- Second meter displays the speed of the plane
>- Third meter displays the height of the plane

### Obstacles and enemies

>- Volcanoes are present randomly which makes the game to come to an end if the plane comes in that particular area. DIRECT GAME OVER
>- FlyingEnemies parachutes appear randomly and attack the plane. They continuosly moves toward the sea and disappear if the reach the sea. This makes the score of the player decrease by one. SCORE DECERASES
>- Checkpoints are present randomly and are limited in no. If a player marks all the checkpoint complete then the game is completed. GAME SUCCESSFULLY COMPLETED
>- Cannons are present on top of every checkpoint that attacks the player by shooting the player once its inside a particular range from the cannon. The cannons direction keeps moving according to the plane.      SCORE DECREASES

### Power-ups / Fuel-ups / Score-increments

>- SmokeRings are present randomly in the scene that keeps rotating . If a player crosses a smokeRings it faces a benefit of 10 points.
>- Fuel-ups come when the fue of the plane is about to finish. Taking which filles up the fuel tank of the player.

### Bombs and Missiles

>- Cannons and parachute attack the player with the help of bombs. If the plane collides with a bombs then its score decreases by one.
>- Plane attacks the cannons , and the flying enemies that are parachutes through missiles and droping bombs.
>- Killing cannon enemies gets 5 point while killing flying enemies score 10 pts.

### BONUS
>- Implemented a functioning Compass.