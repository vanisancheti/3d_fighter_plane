#include "main.h"

#ifndef PARACHUTE_H
#define PARACHUTE_H


class Parachute {
public:
    Parachute() {}
    Parachute(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation_z;
    float rotation_y;
    float rotation_x;
    bool exist;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y, float z);
    void tick(int move);
    void set_rotation();
    void set_speed(int move);
private:
    VAO *object;
    VAO *object1;
};

#endif // PARACHUTE_H
