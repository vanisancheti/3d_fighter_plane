#include "volcano.h"
#include "main.h"
using namespace std;

Volcano::Volcano(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_z = 0;
    this->rotation_x = 0;
    this->rotation_y = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    int n=50;    
    GLfloat vertex_buffer_data[2*n*9];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double y0=20.0f,x0=0.0f,z0=5.0f;
    double x1=0.0f,z1=2.0f;
    double y2=0.0f,x2=0.0f,z2=5.0f;
    double x3=0.0f,z3=2.0f;
    for(int i=0;i<n;i++)
    {
     
        z3 =z2*cos(arg)-x2*sin(arg);
        x3 =z2*sin(arg)+x2*cos(arg);
      
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);

        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z0;

        vertex_buffer_data[temp++] = x1;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z1;

        vertex_buffer_data[temp++] = x2;
        vertex_buffer_data[temp++] = y2;
        vertex_buffer_data[temp++] = z2;


        vertex_buffer_data[temp++] = x3;
        vertex_buffer_data[temp++] = y2; 
        vertex_buffer_data[temp++] = z3;

        vertex_buffer_data[temp++] = x1;
        vertex_buffer_data[temp++] = y0;
        vertex_buffer_data[temp++] = z1; 

        vertex_buffer_data[temp++] = x2;
        vertex_buffer_data[temp++] = y2; 
        vertex_buffer_data[temp++] = z2;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }
    int n1=50;    
    GLfloat vertex_buffer_data1[2*n1*9];
    temp=0;
    PI = 3.141592653589793238462643383279502884197169399375105820974944;
    arg = 2*PI/n1;
    x0=0.0f,z0=10.0f,y0=0.0f;
    x1=0.0f,z1=10.0f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data1[temp++] = 0.0f+5.0f;
        vertex_buffer_data1[temp++] = 25.0f; 
        vertex_buffer_data1[temp++] = 0.0f;
        vertex_buffer_data1[temp++] = x0+5.0f;
        vertex_buffer_data1[temp++] = y0; 
        vertex_buffer_data1[temp++] = z0; 
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);
        vertex_buffer_data1[temp++] = x1+5.0f;
        vertex_buffer_data1[temp++] = y0;
        vertex_buffer_data1[temp++] = z1;
        vertex_buffer_data1[temp++] = 0.0f+5.0f;
        vertex_buffer_data1[temp++] = 0.0f; 
        vertex_buffer_data1[temp++] = 0.0f;
        vertex_buffer_data1[temp++] = x0+5.0f;
        vertex_buffer_data1[temp++] = y0; 
        vertex_buffer_data1[temp++] = z0;
        vertex_buffer_data1[temp++] = x1+5.0f;
        vertex_buffer_data1[temp++] = y0;
        vertex_buffer_data1[temp++] = z1; 
        z0 = z1;
        x0 = x1;
    }
    GLfloat vertex_buffer_data2[2*n1*9];
    temp=0;
    PI = 3.141592653589793238462643383279502884197169399375105820974944;
    arg = 2*PI/n1;
    x0=0.0f,z0=10.0f,y0=0.0f;
    x1=0.0f,z1=10.0f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data2[temp++] = 0.0f-5.0f;
        vertex_buffer_data2[temp++] = 25.0f; 
        vertex_buffer_data2[temp++] = 0.0f;
        vertex_buffer_data2[temp++] = x0-5.0f;
        vertex_buffer_data2[temp++] = y0; 
        vertex_buffer_data2[temp++] = z0; 
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);
        vertex_buffer_data2[temp++] = x1-5.0f;
        vertex_buffer_data2[temp++] = y0;
        vertex_buffer_data2[temp++] = z1;
        vertex_buffer_data2[temp++] = 0.0f-5.0f;
        vertex_buffer_data2[temp++] = 0.0f; 
        vertex_buffer_data2[temp++] = 0.0f;
        vertex_buffer_data2[temp++] = x0-5.0f;
        vertex_buffer_data2[temp++] = y0; 
        vertex_buffer_data2[temp++] = z0;
        vertex_buffer_data2[temp++] = x1-5.0f;
        vertex_buffer_data2[temp++] = y0;
        vertex_buffer_data2[temp++] = z1; 
        z0 = z1;
        x0 = x1;
    }
    GLfloat vertex_buffer_data3[2*n1*9];
    temp=0;
    PI = 3.141592653589793238462643383279502884197169399375105820974944;
    arg = 2*PI/n1;
    x0=0.0f,z0=10.0f,y0=0.0f;
    x1=0.0f,z1=10.0f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data3[temp++] = 0.0f;
        vertex_buffer_data3[temp++] = 25.0f; 
        vertex_buffer_data3[temp++] = 0.0f+5.0f;
        vertex_buffer_data3[temp++] = x0;
        vertex_buffer_data3[temp++] = y0; 
        vertex_buffer_data3[temp++] = z0+5.0f; 
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);
        vertex_buffer_data3[temp++] = x1;
        vertex_buffer_data3[temp++] = y0;
        vertex_buffer_data3[temp++] = z1+5.0f;
        vertex_buffer_data3[temp++] = 0.0f;
        vertex_buffer_data3[temp++] = 0.0f; 
        vertex_buffer_data3[temp++] = 0.0f+5.0f;
        vertex_buffer_data3[temp++] = x0;
        vertex_buffer_data3[temp++] = y0; 
        vertex_buffer_data3[temp++] = z0+5.0f;
        vertex_buffer_data3[temp++] = x1;
        vertex_buffer_data3[temp++] = y0;
        vertex_buffer_data3[temp++] = z1+5.0f; 
        z0 = z1;
        x0 = x1;
    }
    GLfloat vertex_buffer_data4[2*n1*9];
    temp=0;
    PI = 3.141592653589793238462643383279502884197169399375105820974944;
    arg = 2*PI/n1;
    x0=0.0f,z0=10.0f,y0=0.0f;
    x1=0.0f,z1=10.0f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data4[temp++] = 0.0f;
        vertex_buffer_data4[temp++] = 25.0f; 
        vertex_buffer_data4[temp++] = 0.0f-5.0f;
        vertex_buffer_data4[temp++] = x0;
        vertex_buffer_data4[temp++] = y0; 
        vertex_buffer_data4[temp++] = z0-5.0f; 
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);
        vertex_buffer_data4[temp++] = x1;
        vertex_buffer_data4[temp++] = y0;
        vertex_buffer_data4[temp++] = z1-5.0f;
        vertex_buffer_data4[temp++] = 0.0f;
        vertex_buffer_data4[temp++] = 0.0f; 
        vertex_buffer_data4[temp++] = 0.0f-5.0f;
        vertex_buffer_data4[temp++] = x0;
        vertex_buffer_data4[temp++] = y0; 
        vertex_buffer_data4[temp++] = z0-5.0f;
        vertex_buffer_data4[temp++] = x1;
        vertex_buffer_data4[temp++] = y0;
        vertex_buffer_data4[temp++] = z1-5.0f; 
        z0 = z1;
        x0 = x1;
    }
    temp=0;
    GLfloat vertex_buffer_data5[n1*9];
    x0=0.0f,y0=0.0f,z0=4.0f;
    x1=0.0f,z1=0.0f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data5[temp++] = 0.0f;
        vertex_buffer_data5[temp++] = 0.0f+20.0f; 
        vertex_buffer_data5[temp++] = 0.0f;
        vertex_buffer_data5[temp++] = x0;
        vertex_buffer_data5[temp++] = y0+20.0f; 
        vertex_buffer_data5[temp++] = z0; 
        x1 = x0*cos(arg)-z0*sin(arg);
        z1 = x0*sin(arg)+z0*cos(arg);
        x0 = x1;
        z0 = z1;
        vertex_buffer_data5[temp++] = x0;
        vertex_buffer_data5[temp++] = y0+20.0f;
        vertex_buffer_data5[temp++] = z0;
    }
    this->object = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data, color, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data1, color, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data2, color, GL_FILL);
    this->object3 = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data3, color, GL_FILL);
    this->object4 = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data4, color, GL_FILL);
    this->object5 = create3DObject(GL_TRIANGLES,n*3, vertex_buffer_data5,COLOR_ORANGE, GL_FILL);
}

void Volcano::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate_x    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,1,0));
    glm::mat4 rotate_z    = glm::rotate((float) (this->rotation_z * M_PI / 180.0f), glm::vec3(0,0,1));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    Matrices.model *= (translate*rotate_y);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
    draw3DObject(this->object3);
    draw3DObject(this->object4);
    draw3DObject(this->object5);
}

void Volcano::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Volcano::tick(int move) {
     
}
void Volcano::set_rotation() {
    // this->rotation_y += 1;
}
void Volcano::set_speed(int move) {

}