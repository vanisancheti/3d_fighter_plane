#include "ground.h"
#include "main.h"
const int MAXN = 200;
Ground::Ground(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    speed = 1;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    // static const GLfloat vertex_buffer_data[] = {
    //     -12000.0f,-500.0f,-12000.0f, // triangle 1 : begin
    //     -12000.0f,-500.0f, 12000.0f,
    //     -12000.0f, -200.0f, 12000.0f, // triangle 1 : end
    //     12000.0f, -200.0f,-12000.0f, // triangle 2 : begin
    //     -12000.0f,-500.0f,-12000.0f,
    //     -12000.0f, -200.0f,-12000.0f, // triangle 2 : end
    //     12000.0f,-500.0f, 12000.0f,
    //     -12000.0f,-500.0f,-12000.0f,
    //     12000.0f,-500.0f,-12000.0f,
    //     12000.0f, -200.0f,-12000.0f,
    //     12000.0f,-500.0f,-12000.0f,
    //     -12000.0f,-500.0f,-12000.0f,
    //     -12000.0f,-500.0f,-12000.0f,
    //     -12000.0f, -200.0f, 12000.0f,
    //     -12000.0f, -200.0f,-12000.0f,
    //     12000.0f,-500.0f, 12000.0f,
    //     -12000.0f,-500.0f, 12000.0f,
    //     -12000.0f,-500.0f,-12000.0f,
    //     -12000.0f, -200.0f, 12000.0f,
    //     -12000.0f,-500.0f, 12000.0f,
    //     12000.0f,-500.0f, 12000.0f,
    //     12000.0f, -200.0f, 12000.0f,
    //     12000.0f,-500.0f,-12000.0f,
    //     12000.0f, -200.0f,-12000.0f,
    //     12000.0f,-500.0f,-12000.0f,
    //     12000.0f, -200.0f, 12000.0f,
    //     12000.0f,-500.0f, 12000.0f,
    //     12000.0f, -200.0f, 12000.0f,
    //     12000.0f, -200.0f,-12000.0f,
    //     -12000.0f, -200.0f,-12000.0f,
    //     12000.0f, -200.0f, 12000.0f,
    //     -12000.0f, -200.0f,-12000.0f,
    //     -12000.0f, -200.0f, 12000.0f,
    //     12000.0f, -200.0f, 12000.0f,
    //     -12000.0f, -200.0f, 12000.0f,
    //     12000.0f,-500.0f, 12000.0f
    // };
    static const GLfloat vertex_buffer_data[] = {
        -12000.0f,0.0f,-12000.0f,
        -12000.0f,0.0f,12000.0f,
        12000.0f, 0.0f,-12000.0f,
        12000.0f, 0.f,-12000.0f,
        -12000.0f,0.0f,12000.0f,
        12000.0f, 0.0f,12000.0f
    };
    this->object = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data, color, GL_FILL);

    // const int n = 100;
    // const int reqd = n*360;
    // GLfloat vertex_buffer_data[reqd] = {};

    // double angle =0;
    // const double pi = 4*atan(1);
    // double diff = (2*pi)/(double)n;
    // int cur =0;

    // for(int j=-20;j< 20;j++)
    // {
    // for (int i=0;i<n;i++)
    // {
    //     //Origin
    //     vertex_buffer_data[cur++]=0.0;
    //     vertex_buffer_data[cur++]= (float)j; 
    //     vertex_buffer_data[cur++]=0.0f;

    //     //Point with lower angle
    //     vertex_buffer_data[cur++]=INT_MAX*cos(angle);
    //     vertex_buffer_data[cur++]= (float)j;

    //     vertex_buffer_data[cur++]=INT_MAX*sin(angle);
        

    //     //Point with higher angle
    //     angle+= diff;
    //     vertex_buffer_data[cur++]=INT_MAX*cos(angle);
    //     vertex_buffer_data[cur++]= (float)j;

    //     vertex_buffer_data[cur++]=INT_MAX*sin(angle);
    // }
    // }

  
    // this->object = create3DObject(GL_TRIANGLES, reqd/3, vertex_buffer_data, color, GL_FILL);


}

void Ground::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1*translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Ground::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Ground::tick() {
    this->rotation += speed;
    // this->position.x -= speed;
    // this->position.y -= speed;
}

