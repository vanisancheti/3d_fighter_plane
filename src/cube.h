#include "main.h"

#ifndef CUBE_H
#define CUBE_H


class Cube {
public:
    Cube() {}
    Cube(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation_y;
    float rotation_x;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y, float z);
    void tick(int move);
    void set_rotation_y(int move);
    void set_rotation_x(int move);
    double speed;
    double fuel;
    int score;
private:
    VAO *object;
    VAO *object1;
    VAO *object2;
    VAO *object3;
    VAO *object4;
    VAO *object5;
    VAO *object6;
};

#endif // CUBE_H
