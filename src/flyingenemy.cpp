#include "flyingenemy.h"
#include "main.h"
#include "parachute.h"
using namespace std;
Parachute P11;
Parachute P21;
vector<Parachute> Parachutes;
vector<Parachute>::iterator Pit;
Flyingenemy::Flyingenemy(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_z = 0;
    this->rotation_x = 0;
    this->rotation_y = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // 
    // this->object = create3DObject(GL_TRIANGLES,2*n*3/3, vertex_buffer_data1, color, GL_FILL);
    // P11 = Parachute(this->position.x,this->position.y,this->position.z,COLOR_GOLD);
    // P21 = Parachute(this->position.x,this->position.y,this->position.z,COLOR_GOLD);
    int n=50;    
    GLfloat vertex_buffer_data4[2*n*9];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double y0=5.0f,x0=0.0f,z0=0.5f;
    double x1=0.0f,z1=2.0f;
    double y2=-15.0f,x2=0.0f,z2=0.5f;
    double x3=0.0f,z3=2.0f;
    for(int i=0;i<n;i++)
    {
     
        z3 =z2*cos(arg)-x2*sin(arg);
        x3 =z2*sin(arg)+x2*cos(arg);
      
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);

        vertex_buffer_data4[temp++] = x0-0.0f;
        vertex_buffer_data4[temp++] = y0-7.0f; 
        vertex_buffer_data4[temp++] = z0-7.0f;

        vertex_buffer_data4[temp++] = x1-0.0f;
        vertex_buffer_data4[temp++] = y0-7.0f; 
        vertex_buffer_data4[temp++] = z1-7.0f;

        vertex_buffer_data4[temp++] = x2-0.0f;
        vertex_buffer_data4[temp++] = y2-7.0f;
        vertex_buffer_data4[temp++] = z2-7.0f;


        vertex_buffer_data4[temp++] = x3-0.0f;
        vertex_buffer_data4[temp++] = y2-7.0f; 
        vertex_buffer_data4[temp++] = z3-7.0f;

        vertex_buffer_data4[temp++] = x1-0.0f;
        vertex_buffer_data4[temp++] = y0-7.0f;
        vertex_buffer_data4[temp++] = z1-7.0f; 

        vertex_buffer_data4[temp++] = x2-0.0f;
        vertex_buffer_data4[temp++] = y2-7.0f; 
        vertex_buffer_data4[temp++] = z2-7.0f;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }

    GLfloat vertex_buffer_data1[2*n*9];
    temp=0;
    y0=5.0f,x0=0.0f,z0=0.5f;
    x1=0.0f,z1=2.0f;
    y2=-15.0f,x2=0.0f,z2=0.5f;
    x3=0.0f,z3=2.0f;
    for(int i=0;i<n;i++)
    {
     
        z3 =z2*cos(arg)-x2*sin(arg);
        x3 =z2*sin(arg)+x2*cos(arg);
      
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);

        vertex_buffer_data1[temp++] = x0+0.0f;
        vertex_buffer_data1[temp++] = y0-7.0f; 
        vertex_buffer_data1[temp++] = z0+7.0f;

        vertex_buffer_data1[temp++] = x1+0.0f;
        vertex_buffer_data1[temp++] = y0-7.0f; 
        vertex_buffer_data1[temp++] = z1+7.0f;

        vertex_buffer_data1[temp++] = x2+0.0f;
        vertex_buffer_data1[temp++] = y2-7.0f;
        vertex_buffer_data1[temp++] = z2+7.0f;


        vertex_buffer_data1[temp++] = x3+0.0f;
        vertex_buffer_data1[temp++] = y2-7.0f; 
        vertex_buffer_data1[temp++] = z3+7.0f;

        vertex_buffer_data1[temp++] = x1+0.0f;
        vertex_buffer_data1[temp++] = y0-7.0f;
        vertex_buffer_data1[temp++] = z1+7.0f; 

        vertex_buffer_data1[temp++] = x2+0.0f;
        vertex_buffer_data1[temp++] = y2-7.0f; 
        vertex_buffer_data1[temp++] = z2+7.0f;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }
    static const GLfloat vertex_buffer_data[] = {
        -7.0f,-22.0f,-3.0f, // triangle 1 : begin
        -7.0f,-22.0f, 8.0f,
        -7.0f, -25.0f, 8.0f, // triangle 1 : end
         
        -7.0f,-22.0f,-3.0f,
        -7.0f, -25.0f, 8.0f,
        -7.0f, -25.0f,-3.0f,

        //
         0.0f, -25.0f,-3.0f, // triangle 2 : begin
        -7.0f,-22.0f,-3.0f,
        -7.0f, -25.0f,-3.0f, // triangle 2 : end
        
         0.0f,-22.0f, 8.0f,
        -7.0f,-22.0f, 8.0f,
        -7.0f,-22.0f,-3.0f,
        
        -7.0f, -25.0f, 8.0f,
        -7.0f,-22.0f, 8.0f,
         0.0f,-22.0f, 8.0f,
        
        0.0f, -25.0f, 8.0f,
        0.0f,-22.0f,-3.0f,
        0.0f, -25.0f,-3.0f,
        
        0.0f,-22.0f,-3.0f,
        0.0f, -25.0f, 8.0f,
        0.0f,-22.0f, 8.0f,
        
         0.0f, -25.0f, 8.0f,
        -7.0f, -25.0f, 8.0f,
         0.0f,-22.0f, 8.0f,

         0.0f,-22.0f, 8.0f,
        -7.0f,-22.0f,-3.0f,
         0.0f,-22.0f,-3.0f,
        
         0.0f, -25.0f,-3.0f,
         0.0f,-22.0f,-3.0f,
        -7.0f,-22.0f,-3.0f,

         0.0f, -25.0f, 8.0f,
         0.0f, -25.0f,-3.0f,
        -7.0f, -25.0f,-3.0f,
        
         0.0f, -25.0f, 8.0f,
        -7.0f, -25.0f,-3.0f,
        -7.0f, -25.0f, 8.0f,
    };


    Parachute temp1;
    arg=0;

    for(int i=0;i<20;i++)
    {
        temp1 = Parachute(this->position.x,this->position.y,this->position.z,COLOR_GOLD);
        arg+=360/20;
        temp1.rotation_y = arg;
        Parachutes.push_back(temp1);
    }

    this->object = create3DObject(GL_TRIANGLES,2*3*n, vertex_buffer_data4, COLOR_BLACK, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES,2*3*n, vertex_buffer_data1, COLOR_BLACK, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES,12*3, vertex_buffer_data, COLOR_BLACK, GL_FILL);
}

void Flyingenemy::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);
    rotation_y=-10;    // glTranslatef
    glm::mat4 rotate_x    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_z    = glm::rotate((float) (this->rotation_z * M_PI / 180.0f), glm::vec3(0,0,1));
    glm::mat4 scale1 = glm::scale(scale);
    // // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    Matrices.model *= (translate*rotate_y);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    rotation_y=+10;    // glTranslatef
    Matrices.model = glm::mat4(1.0f);
    rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(1,1,0));
    Matrices.model *= (translate*rotate_y);
    MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
    
    // P1.rotation_y = rotation_y;
    // rotation_y++;
    // P21.rotation_y = 90;
    Pit = Parachutes.begin()+this->n;
    while(Pit!=Parachutes.begin()+this->n+20)
    {
        // Pit->tick(0);
        // Pit->set_position(Pit->position.x,this->position.y,Pit->position.z);
        Pit->draw(VP,scale);
        Pit++;
    }
    // P11.draw(VP,scale);
    // P21.draw(VP,scale);
}

void Flyingenemy::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Flyingenemy::tick(int move) {
        Pit = Parachutes.begin()+this->n;
        while(Pit!=Parachutes.begin()+this->n+20)
        {
            Pit->set_position(Pit->position.x,this->position.y,Pit->position.z);
            Pit++;
        }

    this->position.y-=0.1;
}
void Flyingenemy::set_rotation() {
    // this->rotation_y += 1;
}
void Flyingenemy::erase1() {
    if(!this->exist)
    {
        Pit = Parachutes.begin()+this->n;
        int n=0;
        while(n!=20)
        {
            // cout << " ERASE" << endl;
            Parachutes.erase(Pit);
            n++;
        }   
    }
}
void Flyingenemy::set_speed(int move) {

}