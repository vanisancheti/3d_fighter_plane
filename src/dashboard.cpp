#include "dashboard.h"
#include "main.h"
#include "speed.h"
using namespace std;
Dashboard::Dashboard(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_y1 = 90;
    this->rotation_y2 = -90;
    this->rotation_y3 = 30;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
   
    int n=50;    
    GLfloat vertex_buffer_data1[n*9];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double y0=0.6f,x0=1.5f,z0=0.0f;
    double x1=0.0f,z1=3.0f,y1;
    double y2=0.0f,x2=1.5f,z2=0.0f;
    double x3=0.0f,z3=3.0f,y3;
    for(int i=0;i<n/2;i++)
    {
     
        z3 =z2*cos(-arg)-x2*sin(-arg);
        x3 =z2*sin(-arg)+x2*cos(-arg);
      
        z1 =z0*cos(-arg)-x0*sin(-arg);
        x1 =z0*sin(-arg)+x0*cos(-arg);

        vertex_buffer_data1[temp++] = x0-1.75f;
        vertex_buffer_data1[temp++] = y0-3.5f; 
        vertex_buffer_data1[temp++] = z0-8.0f;

        vertex_buffer_data1[temp++] = x1-1.75f;
        vertex_buffer_data1[temp++] = y0-3.5f; 
        vertex_buffer_data1[temp++] = z1-8.0f;

        vertex_buffer_data1[temp++] = x2-1.75f;
        vertex_buffer_data1[temp++] = y2-3.5f;
        vertex_buffer_data1[temp++] = z2-8.0f;

        vertex_buffer_data1[temp++] = x3-1.75f;
        vertex_buffer_data1[temp++] = y2-3.5f; 
        vertex_buffer_data1[temp++] = z3-8.0f;

        vertex_buffer_data1[temp++] = x1-1.75f;
        vertex_buffer_data1[temp++] = y0-3.5f;
        vertex_buffer_data1[temp++] = z1-8.0f; 

        vertex_buffer_data1[temp++] = x2-1.75f;
        vertex_buffer_data1[temp++] = y2-3.5f; 
        vertex_buffer_data1[temp++] = z2-8.0f;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }

    this->object1 = create3DObject(GL_TRIANGLES,n*3, vertex_buffer_data1,COLOR_BLACK, GL_FILL);
}

void Dashboard::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation_y1 * M_PI / 180.0f), glm::vec3(0,1, 0));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    
    Matrices.model *= (translate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object1);
    Speed S1,S2,S3;
    S1 = Speed(-0.75,-1.0f,-3.0f,COLOR_WHITE);
    S2 = Speed(-1.05,-1.0f,-3.0f,COLOR_WHITE);
    S3 = Speed(-0.45,-1.0f,-3.0f,COLOR_WHITE);
    S1.rotation_y = this->rotation_y1;
    S2.rotation_y = this->rotation_y2;
    S3.rotation_y = this->rotation_y3;
    
    S1.draw(VP,scale,0);
    S2.draw(VP,scale,0);
    S3.draw(VP,scale,0);
}

void Dashboard::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Dashboard::tick(int move) {
   //  if(move == 0)
   //  {
   //      this->position.x -=speed*cos(this->rotation_y * M_PI / 180.0f);
   //      this->position.z += speed*sin(this->rotation_y * M_PI / 180.0f);
   //  }
   //  if(move == 1)
   //  {
   //     this->position.y -= speed;
       
   //  }
   //  if(move == 2)
   //  {
   //      this->position.x +=speed*cos(this->rotation_y * M_PI / 180.0f);
   //       this->position.z -=speed*sin(this->rotation_y * M_PI / 180.0f);
   //  }
   //  if(move == 3)
   //     this->position.y += speed;
   // if(move == 4)
   //      this->position.z += speed*sin(this->rotation_y * M_PI / 180.0f);
   //  if(move == 5)
   //      this->position.z -=speed*sin(this->rotation_y * M_PI / 180.0f);
}
void Dashboard::set_rotation(int move,double val) {
    
    if(rotation_y1<90 && move==1)
    this->rotation_y1++;
    if(rotation_y1>-90 && move==2)
    this->rotation_y1--;

    if(rotation_y2<90 && move==3)
    this->rotation_y2+=0.01;
    if(rotation_y2>-90 && move==4)
    this->rotation_y2-=0.01;
    
    if(rotation_y3<90 && move==5)
    this->rotation_y3+=val;
    if(rotation_y3>-90 && move==6)
    this->rotation_y3-=val;
}