#include "island.h"
#include "main.h"
using namespace std;


Island::Island(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    speed = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    int n=50;    
    GLfloat vertex_buffer_data[2*n*9];
    int temp=0;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    double x0=0.0f,z0=20.0f,y0=0.0f;
    double x1=0.0f,z1=20.0f;
    for(int i=0;i<n;i++)
    {
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = 20.0f; 
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z0; 
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);
        vertex_buffer_data[temp++] = x1;
        vertex_buffer_data[temp++] = y0;
        vertex_buffer_data[temp++] = z1;
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = 0.0f; 
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z0;
        vertex_buffer_data[temp++] = x1;
        vertex_buffer_data[temp++] = y0;
        vertex_buffer_data[temp++] = z1; 
        z0 = z1;
        x0 = x1;
    }
    // static const GLfloat vertex_buffer_data[] = {
    //     -5.0f,-5.0f,-5.0f, // triangle 1 : begin
    //     -5.0f,-5.0f, 5.0f,
    //     -5.0f, 5.0f, 5.0f, // triangle 1 : end
    //     5.0f, 5.0f,-5.0f, // triangle 2 : begin
    //     -5.0f,-5.0f,-5.0f,
    //     -5.0f, 5.0f,-5.0f, // triangle 2 : end
    //     5.0f,-5.0f, 5.0f,
    //     -5.0f,-5.0f,-5.0f,
    //     5.0f,-5.0f,-5.0f,
    //     5.0f, 5.0f,-5.0f,
    //     5.0f,-5.0f,-5.0f,
    //     -5.0f,-5.0f,-5.0f,
    //     -5.0f,-5.0f,-5.0f,
    //     -5.0f, 5.0f, 5.0f,
    //     -5.0f, 5.0f,-5.0f,
    //     5.0f,-5.0f, 5.0f,
    //     -5.0f,-5.0f, 5.0f,
    //     -5.0f,-5.0f,-5.0f,
    //     -5.0f, 5.0f, 5.0f,
    //     -5.0f,-5.0f, 5.0f,
    //     5.0f,-5.0f, 5.0f,
    //     5.0f, 5.0f, 5.0f,
    //     5.0f,-5.0f,-5.0f,
    //     5.0f, 5.0f,-5.0f,
    //     5.0f,-5.0f,-5.0f,
    //     5.0f, 5.0f, 5.0f,
    //     5.0f,-5.0f, 5.0f,
    //     5.0f, 5.0f, 5.0f,
    //     5.0f, 5.0f,-5.0f,
    //     -5.0f, 5.0f,-5.0f,
    //     5.0f, 5.0f, 5.0f,
    //     -5.0f, 5.0f,-5.0f,
    //     -5.0f, 5.0f, 5.0f,
    //     5.0f, 5.0f, 5.0f,
    //     -5.0f, 5.0f, 5.0f,
    //     5.0f,-5.0f, 5.0f
    // };


    // for(int i=0;i<n1;i++)
    // {
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = 0.0f; 
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0; 
    //     vertex_buffer_data[temp++] = z0; 
    //     x1 = x0*cos(arg)-y0*sin(arg);
    //     y1 = x0*sin(arg)+y0*cos(arg);
    //     x0 = x1;
    //     y0 = y1;
    //     vertex_buffer_data[temp++] = x1;
    //     vertex_buffer_data[temp++] = y1;
    //     vertex_buffer_data[temp++] = z0;
    //     // vertex_buffer_data[temp++] = 0.0f;
    //     // vertex_buffer_data[temp++] = 0.0f; 
    //     // vertex_buffer_data[temp++] = 2.0f;
    //     // vertex_buffer_data[temp++] = x0;
    //     // vertex_buffer_data[temp++] = y0; 
    //     // vertex_buffer_data[temp++] = 2.0f; 
    //     // vertex_buffer_data[temp++] = x1;
    //     // vertex_buffer_data[temp++] = y1;
    //     // vertex_buffer_data[temp++] = 2.0f;
        
    // }

    this->object = create3DObject(GL_TRIANGLES,2*n*3, vertex_buffer_data,color, GL_FILL);
}

void Island::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0,1, 0));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate*rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Island::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Island::tick(int move) {
    if(move == 0)
    {
        this->position.x -=speed*cos(this->rotation * M_PI / 180.0f);
        this->position.z += speed*sin(this->rotation * M_PI / 180.0f);
    }
    if(move == 1)
    {
       this->position.y -= speed;
       
    }
    if(move == 2)
    {
        this->position.x +=speed*cos(this->rotation * M_PI / 180.0f);
         this->position.z -=speed*sin(this->rotation * M_PI / 180.0f);
    }
    if(move == 3)
       this->position.y += speed;
   if(move == 4)
        this->position.z += speed*sin(this->rotation * M_PI / 180.0f);
    if(move == 5)
        this->position.z -=speed*sin(this->rotation * M_PI / 180.0f);
}
void Island::set_rotation(int move) {
    if(move==0)
     this->rotation+=1;
    else
     this->rotation-=1;
}