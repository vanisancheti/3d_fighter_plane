#include "cannon.h"
#include "main.h"

Cannon::Cannon(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_z = 0;
    this->rotation_x = 0;
    this->rotation_y = 0;
    speed = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    
    int n=100;    
    GLfloat vertex_buffer_data1[3*n*9];
    int temp=0;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    double x0=-5.0f,y0=0.0f,z0=4.0f;
    double y1=0.0f,z1=4.0f;

    double x2=5.0f,y2=0.0f,z2=2.0f;
    double y3=0.0f,z3=4.0f;
    for(int i=0;i<n;i++)
    {
        vertex_buffer_data1[temp++] = 5.0f;
        vertex_buffer_data1[temp++] = 0.0f; 
        vertex_buffer_data1[temp++] = 0.0f;
        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y2; 
        vertex_buffer_data1[temp++] = z2; 
        z3 =z2*cos(arg)-y2*sin(arg);
        y3 =z2*sin(arg)+y2*cos(arg);
      
        z1 =z0*cos(arg)-y0*sin(arg);
        y1 =z0*sin(arg)+y0*cos(arg);
        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y3;
        vertex_buffer_data1[temp++] = z3;

        vertex_buffer_data1[temp++] = x0;
        vertex_buffer_data1[temp++] = y0; 
        vertex_buffer_data1[temp++] = z0;

        vertex_buffer_data1[temp++] = x0;
        vertex_buffer_data1[temp++] = y1; 
        vertex_buffer_data1[temp++] = z1;

        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y2;
        vertex_buffer_data1[temp++] = z2;


        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y3; 
        vertex_buffer_data1[temp++] = z3;

        vertex_buffer_data1[temp++] = x0;
        vertex_buffer_data1[temp++] = y1;
        vertex_buffer_data1[temp++] = z1; 

        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y2; 
        vertex_buffer_data1[temp++] = z2;

        z0 = z1;
        y0 = y1;
        z2 = z3;
        y2 = y3;
    }
    
    this->object = create3DObject(GL_TRIANGLES,3*n*3, vertex_buffer_data1,COLOR_GREEN, GL_FILL);
}

void Cannon::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate_x    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,1,0));
    glm::mat4 rotate_z    = glm::rotate((float) (this->rotation_z * M_PI / 180.0f), glm::vec3(0,0,1));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    Matrices.model *= (translate*rotate_z*rotate_x*rotate_y);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Cannon::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Cannon::tick(int move) {

}
void Cannon::set_rotation(int move) {
    if(move==0)
     this->rotation_x+=1;
    if(move==1)
     this->rotation_x-=1;

    if(move==2)
     this->rotation_y+=1;
    if(move==3)
     this->rotation_y-=1;

    if(move==4)
     this->rotation_z+=1;
    if(move==5)
     this->rotation_z-=1;
}