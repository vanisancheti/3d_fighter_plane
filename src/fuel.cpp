#include "fuel.h"
#include "main.h"
using namespace std;

Fuel::Fuel(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_z = 0;
    this->rotation_x = 0;
    this->rotation_y = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
        -1.0f,-1.0f, 1.0f, // triangle 1 : begin
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 5.0f, // triangle 1 : end
         
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 5.0f,
        -1.0f,-1.0f, 5.0f,

        //
         1.0f,-1.0f, 5.0f, // triangle 2 : begin
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 5.0f, // triangle 2 : end
        
         1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        
        -1.0f, 1.0f, 5.0f,
        -1.0f, 1.0f, 1.0f,
         1.0f, 1.0f, 1.0f,
        
         1.0f, 1.0f, 5.0f,
         1.0f,-1.0f, 1.0f,
         1.0f,-1.0f, 5.0f,
        
         1.0f,-1.0f, 1.0f,
         1.0f, 1.0f, 5.0f,
         1.0f, 1.0f, 1.0f,
        
         1.0f, 1.0f, 5.0f,
        -1.0f, 1.0f, 5.0f,
         1.0f, 1.0f, 1.0f,

         1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
         1.0f,-1.0f, 1.0f,
        
         1.0f,-1.0f, 5.0f,
         1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,

         1.0f, 1.0f, 5.0f,
         1.0f,-1.0f, 5.0f,
        -1.0f,-1.0f, 5.0f,
        
         1.0f, 1.0f, 5.0f,
        -1.0f,-1.0f, 5.0f,
        -1.0f, 1.0f, 5.0f,
    };
    static const GLfloat vertex_buffer_data2[] = {
        -1.0f, 3.0f, 1.0f, // triangle 1 : begin
        -1.0f, 5.0f, 1.0f,
        -1.0f, 5.0f, 5.0f, // triangle 1 : end
         
        -1.0f, 3.0f, 1.0f,
        -1.0f, 5.0f, 5.0f,
        -1.0f, 3.0f, 5.0f,

        //
         1.0f, 3.0f, 5.0f, // triangle 2 : begin
        -1.0f, 3.0f, 1.0f,
        -1.0f, 3.0f, 5.0f, // triangle 2 : end
        
         1.0f, 5.0f, 1.0f,
        -1.0f, 5.0f, 1.0f,
        -1.0f, 3.0f, 1.0f,
        
        -1.0f, 5.0f, 5.0f,
        -1.0f, 5.0f, 1.0f,
         1.0f, 5.0f, 1.0f,
        
         1.0f, 5.0f, 5.0f,
         1.0f, 3.0f, 1.0f,
         1.0f, 3.0f, 5.0f,
        
         1.0f, 3.0f, 1.0f,
         1.0f, 5.0f, 5.0f,
         1.0f, 5.0f, 1.0f,
        
         1.0f, 5.0f, 5.0f,
        -1.0f, 5.0f, 5.0f,
         1.0f, 5.0f, 1.0f,

         1.0f, 5.0f, 1.0f,
        -1.0f, 3.0f, 1.0f,
         1.0f, 3.0f, 1.0f,
        
         1.0f, 3.0f, 5.0f,
         1.0f, 3.0f, 1.0f,
        -1.0f, 3.0f, 1.0f,

         1.0f, 5.0f, 5.0f,
         1.0f, 3.0f, 5.0f,
        -1.0f, 3.0f, 5.0f,
        
         1.0f, 5.0f, 5.0f,
        -1.0f, 3.0f, 5.0f,
        -1.0f, 5.0f, 5.0f,
    };
    static const GLfloat vertex_buffer_data1[] = {
        -1.0f,-5.0f,-1.0f, // triangle 1 : begin
        -1.0f,-5.0f, 1.0f,
        -1.0f, 5.0f, 1.0f, // triangle 1 : end
         
        -1.0f,-5.0f,-1.0f,
        -1.0f, 5.0f, 1.0f,
        -1.0f, 5.0f,-1.0f,

        //
         1.0f, 5.0f,-1.0f, // triangle 2 : begin
        -1.0f,-5.0f,-1.0f,
        -1.0f, 5.0f,-1.0f, // triangle 2 : end
        
         1.0f,-5.0f, 1.0f,
        -1.0f,-5.0f, 1.0f,
        -1.0f,-5.0f,-1.0f,
        
        -1.0f, 5.0f, 1.0f,
        -1.0f,-5.0f, 1.0f,
         1.0f,-5.0f, 1.0f,
        
         1.0f, 5.0f, 1.0f,
         1.0f,-5.0f,-1.0f,
         1.0f, 5.0f,-1.0f,
        
         1.0f,-5.0f,-1.0f,
         1.0f, 5.0f, 1.0f,
         1.0f,-5.0f, 1.0f,
        
         1.0f, 5.0f, 1.0f,
        -1.0f, 5.0f, 1.0f,
         1.0f,-5.0f, 1.0f,

         1.0f,-5.0f, 1.0f,
        -1.0f,-5.0f,-1.0f,
         1.0f,-5.0f,-1.0f,
        
         1.0f, 5.0f,-1.0f,
         1.0f,-5.0f,-1.0f,
        -1.0f,-5.0f,-1.0f,

         1.0f, 5.0f, 1.0f,
         1.0f, 5.0f,-1.0f,
        -1.0f, 5.0f,-1.0f,
        
         1.0f, 5.0f, 1.0f,
        -1.0f, 5.0f,-1.0f,
        -1.0f, 5.0f, 1.0f,
    };
   
   
   
    this->object = create3DObject(GL_TRIANGLES,12*3, vertex_buffer_data, color, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES,12*3, vertex_buffer_data1, color, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES,12*3, vertex_buffer_data2, color, GL_FILL);
}

void Fuel::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate_x    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,1,0));
    glm::mat4 rotate_z    = glm::rotate((float) (this->rotation_z * M_PI / 180.0f), glm::vec3(0,0,1));
    glm::mat4 scale1 = glm::scale(scale);
    rotation_y++;
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    Matrices.model *= (translate*rotate_y);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
}

void Fuel::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Fuel::tick(int move) {
     
}
void Fuel::set_rotation() {
    this->rotation_y += 1;
}
void Fuel::set_speed(int move) {

}