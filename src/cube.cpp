#include "cube.h"
#include "main.h"
using namespace std;

Cube::Cube(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_y = 0;
    this->rotation_x = 0;
    speed = 0.5;
    fuel=100;
    score=10;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data[] = {
        -15.0f,-3.0f,-5.0f, // triangle 1 : begin
        -15.0f,-3.0f, 5.0f,
        -15.0f, 3.0f, 5.0f, // triangle 1 : end
         
        -15.0f,-3.0f,-5.0f,
        -15.0f, 3.0f, 5.0f,
        -15.0f, 3.0f,-5.0f,

        //
         8.0f, 3.0f,-5.0f, // triangle 2 : begin
        -15.0f,-3.0f,-5.0f,
        -15.0f, 3.0f,-5.0f, // triangle 2 : end
        
         8.0f,-3.0f, 5.0f,
        -15.0f,-3.0f, 5.0f,
        -15.0f,-3.0f,-5.0f,
        
        -15.0f, 3.0f, 5.0f,
        -15.0f,-3.0f, 5.0f,
         8.0f,-3.0f, 5.0f,
        
        8.0f, 3.0f, 5.0f,
        8.0f,-3.0f,-5.0f,
        8.0f, 3.0f,-5.0f,
        
        8.0f,-3.0f,-5.0f,
        8.0f, 3.0f, 5.0f,
        8.0f,-3.0f, 5.0f,
        
         8.0f, 3.0f, 5.0f,
        -15.0f, 3.0f, 5.0f,
         8.0f,-3.0f, 5.0f,

         8.0f,-3.0f, 5.0f,
        -15.0f,-3.0f,-5.0f,
         8.0f,-3.0f,-5.0f,
        
         8.0f, 3.0f,-5.0f,
         8.0f,-3.0f,-5.0f,
        -15.0f,-3.0f,-5.0f,

         8.0f, 3.0f, 5.0f,
         8.0f, 3.0f,-5.0f,
        -15.0f, 3.0f,-5.0f,
        
         8.0f, 3.0f, 5.0f,
        -15.0f, 3.0f,-5.0f,
        -15.0f, 3.0f, 5.0f,
    };

    static const GLfloat vertex_buffer_data1[] = {
        20.0f,0.0f,0.0f,
        8.0f,3.0f,5.0f,
        8.0f,3.0f,-5.0f,

        20.0f,0.0f,0.0f,
        8.0f,-3.0f,5.0f,
        8.0f,-3.0f,-5.0f,
        
        8.0f,3.0f,5.0f,
        8.0f,-3.0f,5.0f,
        20.0f,0.0f,0.0f,

        8.0f,3.0f,-5.0f,
        8.0f,-3.0f,-5.0f,
        20.0f,0.0f,0.0f
    };

    static const GLfloat vertex_buffer_data2[] = {
        4.0f,-5.0f,30.0f, // triangle 1 : begin
        4.0f,-5.0f,-30.0f,
        4.0f, -3.0f,-30.0f, // triangle 1 : end
        8.0f,-3.0f,30.0f, // triangle 2 : begin
        4.0f,-5.0f,30.0f,
        4.0f, -3.0f,30.0f, // triangle 2 : end
        8.0f,-5.0f,-30.0f,
        4.0f,-5.0f,30.0f,
        8.0f,-5.0f,30.0f,
        8.0f, -3.0f,30.0f,
        8.0f,-5.0f,30.0f,
        4.0f,-5.0f,30.0f,
        4.0f,-5.0f,30.0f,
        4.0f, -3.0f,-30.0f,
        4.0f, -3.0f,30.0f,
        8.0f,-5.0f,-30.0f,
        4.0f,-5.0f,-30.0f,
        4.0f,-5.0f,30.0f,
        4.0f, -3.0f,-30.0f,
        4.0f,-5.0f,-30.0f,
        8.0f,-5.0f,-30.0f,
        8.0f, -3.0f,-30.0f,
        8.0f,-5.0f,30.0f,
        8.0f, -3.0f,30.0f,
        8.0f,-5.0f,30.0f,
        8.0f, -3.0f,-30.0f,
        8.0f,-5.0f,-30.0f,
        8.0f, -3.0f,-30.0f,
        8.0f, -3.0f,30.0f,
        4.0f, -3.0f,30.0f,
        8.0f, -3.0f,-30.0f,
        4.0f, -3.0f,30.0f,
        4.0f, -3.0f,-30.0f,
        8.0f, -3.0f,-30.0f,
        4.0f, -3.0f,-30.0f,
        8.0f,-5.0f,-30.0f 
        };

    static const GLfloat vertex_buffer_data3[] = {
        4.0f, 4.0f,25.0f, // triangle 1 : begin
        4.0f, 4.0f,-25.0f,
        4.0f, 6.0f,-25.0f, // triangle 1 : end
        8.0f, 6.0f,25.0f, // triangle 2 : begin
        4.0f, 4.0f,25.0f,
        4.0f, 6.0f,25.0f, // triangle 2 : end
        8.0f, 4.0f,-25.0f,
        4.0f, 4.0f,25.0f,
        8.0f, 4.0f,25.0f,
        8.0f, 6.0f,25.0f,
        8.0f, 4.0f,25.0f,
        4.0f, 4.0f,25.0f,
        4.0f, 4.0f,25.0f,
        4.0f, 6.0f,-25.0f,
        4.0f, 6.0f,25.0f,
        8.0f, 4.0f,-25.0f,
        4.0f, 4.0f,-25.0f,
        4.0f, 4.0f,25.0f,
        4.0f, 6.0f,-25.0f,
        4.0f, 4.0f,-25.0f,
        8.0f, 4.0f,-25.0f,
        8.0f, 6.0f,-25.0f,
        8.0f, 4.0f,25.0f,
        8.0f, 6.0f,25.0f,
        8.0f, 4.0f,25.0f,
        8.0f, 6.0f,-25.0f,
        8.0f, 4.0f,-25.0f,
        8.0f, 6.0f,-25.0f,
        8.0f, 6.0f,25.0f,
        4.0f, 6.0f,25.0f,
        8.0f, 6.0f,-25.0f,
        4.0f, 6.0f,25.0f,
        4.0f, 6.0f,-25.0f,
        8.0f, 6.0f,-25.0f,
        4.0f, 6.0f,-25.0f,
        8.0f, 6.0f,-25.0f 
        };

    int n=50;    
    GLfloat vertex_buffer_data4[3*n*9];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double y0=4.0f,x0=0.0f,z0=1.0f;
    double x1=0.0f,z1=2.0f;
    double y2=-5.0f,x2=0.0f,z2=1.0f;
    double x3=0.0f,z3=2.0f;
    for(int i=0;i<n;i++)
    {
     
        z3 =z2*cos(arg)-x2*sin(arg);
        x3 =z2*sin(arg)+x2*cos(arg);
      
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);

        vertex_buffer_data4[temp++] = x0+6.0f;
        vertex_buffer_data4[temp++] = y0; 
        vertex_buffer_data4[temp++] = z0+23.0f;

        vertex_buffer_data4[temp++] = x1+6.0f;
        vertex_buffer_data4[temp++] = y0; 
        vertex_buffer_data4[temp++] = z1+23.0f;

        vertex_buffer_data4[temp++] = x2+6.0f;
        vertex_buffer_data4[temp++] = y2;
        vertex_buffer_data4[temp++] = z2+23.0f;


        vertex_buffer_data4[temp++] = x3+6.0f;
        vertex_buffer_data4[temp++] = y2; 
        vertex_buffer_data4[temp++] = z3+23.0f;

        vertex_buffer_data4[temp++] = x1+6.0f;
        vertex_buffer_data4[temp++] = y0;
        vertex_buffer_data4[temp++] = z1+23.0f; 

        vertex_buffer_data4[temp++] = x2+6.0f;
        vertex_buffer_data4[temp++] = y2; 
        vertex_buffer_data4[temp++] = z2+23.0f;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }
    GLfloat vertex_buffer_data5[3*n*9];
    temp=0;
    y0=4.0f,x0=0.0f,z0=1.0f;
    x1=0.0f,z1=2.0f;
    y2=-5.0f,x2=0.0f,z2=1.0f;
    x3=0.0f,z3=2.0f;
    for(int i=0;i<n;i++)
    {
     
        z3 =z2*cos(arg)-x2*sin(arg);
        x3 =z2*sin(arg)+x2*cos(arg);
      
        z1 =z0*cos(arg)-x0*sin(arg);
        x1 =z0*sin(arg)+x0*cos(arg);

        vertex_buffer_data5[temp++] = x0+6.0f;
        vertex_buffer_data5[temp++] = y0; 
        vertex_buffer_data5[temp++] = z0-23.0f;

        vertex_buffer_data5[temp++] = x1+6.0f;
        vertex_buffer_data5[temp++] = y0; 
        vertex_buffer_data5[temp++] = z1-23.0f;

        vertex_buffer_data5[temp++] = x2+6.0f;
        vertex_buffer_data5[temp++] = y2;
        vertex_buffer_data5[temp++] = z2-23.0f;


        vertex_buffer_data5[temp++] = x3+6.0f;
        vertex_buffer_data5[temp++] = y2; 
        vertex_buffer_data5[temp++] = z3-23.0f;

        vertex_buffer_data5[temp++] = x1+6.0f;
        vertex_buffer_data5[temp++] = y0;
        vertex_buffer_data5[temp++] = z1-23.0f; 

        vertex_buffer_data5[temp++] = x2+6.0f;
        vertex_buffer_data5[temp++] = y2; 
        vertex_buffer_data5[temp++] = z2-23.0f;

        z0 = z1;
        x0 = x1;
        z2 = z3;
        x2 = x3;
    }


    this->object6 = create3DObject(GL_TRIANGLES, 2*n*3, vertex_buffer_data5, COLOR_ORANGE, GL_FILL);
    this->object5 = create3DObject(GL_TRIANGLES, 2*n*3, vertex_buffer_data4, COLOR_ORANGE, GL_FILL);
    this->object4 = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data3, COLOR_RED, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data2, COLOR_RED, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES, 4*3, vertex_buffer_data1, COLOR_RED, GL_FILL);
    this->object = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data, COLOR_GREY, GL_FILL);
    this->object3 = create3DObject(GL_TRIANGLES, 4*3, vertex_buffer_data+24,COLOR_GREY, GL_FILL);
}

void Cube::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,1, 0));
    glm::mat4 rotate1    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0, 0));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate*rotate*rotate1);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
    draw3DObject(this->object3);
    draw3DObject(this->object4);
    draw3DObject(this->object5);
    draw3DObject(this->object6);
}

void Cube::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Cube::tick(int move) {
    // cout << this->rotation_y << endl;
    if(move == 0)
    {
        this->position.x -=speed*cos(this->rotation_y * M_PI / 180.0f);
        this->position.z += speed*sin(this->rotation_y * M_PI / 180.0f);
    }
    if(move == 1)
    {
       if(this->position.y > -50)
       this->position.y -= speed;  
    }
    if(move == 2)
    {
        this->position.x +=speed*cos(this->rotation_y * M_PI / 180.0f);
         this->position.z -=speed*sin(this->rotation_y * M_PI / 180.0f);
    }
    if(move == 3)
    {
        if(this->position.y<100)
        this->position.y += speed;
    }
    if(move == 4)
        this->position.z += speed*sin(this->rotation_y * M_PI / 180.0f);
    if(move == 5)
        this->position.z -=speed*sin(this->rotation_y * M_PI / 180.0f);
}
void Cube::set_rotation_y(int move) {
    if(move==0)
     this->rotation_y+=1;
    else
     this->rotation_y-=1;
}
void Cube::set_rotation_x(int move) {
    if(move==0)
     this->rotation_x+=1;
    else
     this->rotation_x-=1;
}