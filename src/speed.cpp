#include "speed.h"
#include "main.h"
using namespace std;


Speed::Speed(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_y = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
   
    int n=50;    
    GLfloat vertex_buffer_data1[n*9];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double y0=0.6f,x0=1.5f,z0=0.0f;
    double x1=0.0f,z1=3.0f,y1;
    double y2=0.0f,x2=1.5f,z2=0.0f;
    double x3=0.0f,z3=3.0f,y3;
    
    GLfloat vertex_buffer_data2[n*9];
    temp=0;
    z0=0.05f,x0=0.1f,y0=0.0f;
    x1=0.0f,y1=3.0f;
    z2=0.0f,x2=0.1f,y2=0.0f;
    x3=0.0f,y3=3.0f;
    for(int i=0;i<n/2;i++)
    {
     
        y3 =y2*cos(-arg)-x2*sin(-arg);
        x3 =y2*sin(-arg)+x2*cos(-arg);
      
        y1 =y0*cos(-arg)-x0*sin(-arg);
        x1 =y0*sin(-arg)+x0*cos(-arg);

        vertex_buffer_data2[temp++] = x0;
        vertex_buffer_data2[temp++] = y0; 
        vertex_buffer_data2[temp++] = z0;

        vertex_buffer_data2[temp++] = x1;
        vertex_buffer_data2[temp++] = y1; 
        vertex_buffer_data2[temp++] = z0;

        vertex_buffer_data2[temp++] = x2;
        vertex_buffer_data2[temp++] = y2;
        vertex_buffer_data2[temp++] = z2;

        vertex_buffer_data2[temp++] = x3;
        vertex_buffer_data2[temp++] = y3; 
        vertex_buffer_data2[temp++] = z2;

        vertex_buffer_data2[temp++] = x1;
        vertex_buffer_data2[temp++] = y1;
        vertex_buffer_data2[temp++] = z0; 

        vertex_buffer_data2[temp++] = x2;
        vertex_buffer_data2[temp++] = y2; 
        vertex_buffer_data2[temp++] = z2;

        y0 = y1;
        x0 = x1;
        y2 = y3;
        x2 = x3;
    }
    static const GLfloat vertex_buffer_data5[] = {
         0.02f,0.0f,0.0f,
        -0.02f,0.0f,0.0f,
         0.0f,0.1f,0.0f
    };
    this->object2 = create3DObject(GL_TRIANGLES,n*3, vertex_buffer_data2,COLOR_WHITE, GL_FILL);
    this->object5 = create3DObject(GL_TRIANGLES,1*3, vertex_buffer_data5,COLOR_WHITE, GL_FILL);
}

void Speed::draw(glm::mat4 VP,glm::vec3 scale,int flag) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate(this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,0,1));
    // this->rotation_y++;
    Matrices.model *= (translate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object2);
    Matrices.model *= (translate*rotate);
    MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object5);
}

void Speed::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Speed::tick(int move) {

}
void Speed::set_rotation(int move) {
}