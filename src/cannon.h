#include "main.h"

#ifndef CANNON_H
#define CANNON_H


class Cannon {
public:
    Cannon() {}
    Cannon(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation_z;
    float rotation_y;
    float rotation_x;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y, float z);
    void tick(int move);
    void set_rotation(int move);
    double speed;
private:
    VAO *object;
    VAO *object1;
};

#endif // CANNON_H
