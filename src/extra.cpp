#include "main.h"
#include "timer.h"
#include "ground.h"
#include "cube.h"
#include "island.h"
#include "arrow.h"
#include "dashboard.h"


using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Cube ball1;
Ground sea;
Island I1;
Arrow A1;
Dashboard D1;
// Dashboard D2;
vector<Island> checkpoint;
int View;
int cnt=0;
int rx,ry,rz;
double mouse_x,mouse_y;
double prevmouse_x,prevmouse_y;
double move_x=0,move_y=0;

// View = 0 Plane View
// View = 1 Top View
// View = 2 Tower View
// View = 3 Follow-cam View
// View = 4 Helicopter-cam View 
float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
Timer t60(1.0 / 60);
Timer t1(1.0 / 30);
double cx,cy,cz,tx,ty,tz;
/* Render the scene with openGL */
/* Edit this function according to your assignment */
double get_camera(int x)
{
    if(x==0)
        return cx;
    if(x==1)
        return cy;
    if(x==2)
        return cz;
    if(x==3)
        return tx;
    if(x==4)
        return ty;
    if(x==5)
        return tz;
}
void change(double a,double b,double c,double d,double e,double f)
{
    cx = a,cy=b,cz=c,tx=d,ty=e,tz=f;
}
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    // glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 0, 5*sin(camera_rotation_angle*M_PI/180.0f) );
    glm::vec3 eye;

    if(View==0)
    {
        cx=ball1.position.x-50*cos(ball1.rotation_y * M_PI / 180.0f);
        cy=ball1.position.y+20;
        cz=ball1.position.z+50*sin(ball1.rotation_y * M_PI / 180.0f);
    }
        // eye = glm::vec3(ball1.position.x,ball1.position.y+20,ball1.position.z);
    if(View==1)
    {
         cx=ball1.position.x,cy=ball1.position.y+500;
         cz=ball1.position.z;
    }
        // eye = glm::vec3( ball1.position.x,100, ball1.position.z);
    if(View==2)
        cx =800,cy=50,cz=800;
    if(View==3)
    {
        cx=ball1.position.x-100*cos(ball1.rotation_y * M_PI / 180.0f);;
        cy=ball1.position.y+20;
        cz=ball1.position.z+100*sin(ball1.rotation_y * M_PI / 180.0f);   
    }
    if(View==4)
    {
        cz=ball1.position.z;
        cx=ball1.position.x;
        cy=ball1.position.y+1000;
    }
        // eye = glm::vec3(100,100,100);

    eye = glm::vec3(cx,cy,cz);

        // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target;
    glm::vec3 up;
    if(View==0)
        tx=ball1.position.x+5*cos(ball1.rotation_y * M_PI / 180.0f),ty=ball1.position.y+10,tz=ball1.position.z-5*sin(ball1.rotation_y * M_PI / 180.0f);
    if(View==1)
        tx=ball1.position.x+0.1,ty=ball1.position.y,tz=ball1.position.z;
        // target = glm::vec3(ball1.position.x+0.1,ball1.position.y,ball1.position.z);
    if(View==2)
        tx=0,ty=50,tz=0;
    if(View==3)
        tx=ball1.position.x,ty=ball1.position.y,tz=ball1.position.z;
    if(View==4)
    {
        tx=ball1.position.x+ move_x,ty=ball1.position.y+move_y,tz=ball1.position.z;
    }
    // target = glm::vec3(100,0,0);

    target = glm::vec3(tx,ty,tz);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up=glm::vec3(0,1,0);


    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!

    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    // Scene render
    glm::vec3 scale = glm::vec3(1.0f,1.0f,1.0f);
    // I1.draw(VP,scale; 
    sea.draw(VP,scale);
    ball1.draw(VP,scale);
    // D1.draw(VP,scale);
    D1.draw(Matrices.projection,scale,0);
    // D2.draw(Matrices.projection,scale,1);
    // A1.draw(VP,scale);
    vector<Island>::iterator it;
    it = checkpoint.begin();
    while(it!=checkpoint.end())
    {
        (*it).draw(VP,scale);
        it++;
    }
    // scale = glm::vec3(10.0f,10.0f,10.0f);
}
void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int up = glfwGetKey(window, GLFW_KEY_UP);
    int down = glfwGetKey(window, GLFW_KEY_DOWN);
    int V = glfwGetKey(window, GLFW_KEY_V);
    //tilt left and right
    int A = glfwGetKey(window, GLFW_KEY_A);
    int D = glfwGetKey(window, GLFW_KEY_D);
    //rotate clockwise and anticlockwise
    int Q = glfwGetKey(window, GLFW_KEY_Q);
    int E = glfwGetKey(window, GLFW_KEY_E);
    

    if(V)
    {
        cnt = (cnt+1)%20;
        // cout << cnt  << " 1"<< endl;
    }
    if(cnt<25)
        View = 4;
    if(cnt<20)
        View = 3;
    if(cnt<15)
        View = 2;
    if(cnt<10)
        View = 1;
    if(cnt<5)
        View = 0;
    // if(up)
    //     View = 0;
    // if (left) {
    //     View = 2;
    // }
    // if(right)
    // {
    //     View = 1;
    // }
    if(A)
    {
        ball1.set_rotation_x(0);
    }
    if(D)
    {
        ball1.set_rotation_x(1);
    }
    if(left)
    {
        ball1.set_rotation_y(0);
        // D1.set_rotation(0);
    }
    if(right)
    {
        ball1.set_rotation_y(1);
        // D1.set_rotation(1);
    }
    if(down)
    {
        ball1.tick(0);
        cy++;
    }
    if(up)
    {
        ball1.tick(2);
        cy--;
    }
}

void tick_elements() {
    // ball1.tick();
    // camera_rotation_angle += 1;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models

    ball1 = Cube(0,-10,0, COLOR_GREEN);
    D1 = Dashboard(500,5,500,COLOR_WHITE);
    // D2 = Dashboard(0,500,0,COLOR_WHITE);
    sea = Ground(0,0,0,COLOR_BLUE);
    A1 = Arrow(cx,cy,cz,COLOR_BLACK);
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 1600;
    int height = 1600;

    window = initGLFW(width, height);

    initGL (window, width, height);
    glfwGetCursorPos(window,&prevmouse_x,&prevmouse_y);
    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
            // 60 fps
            // OpenGL Draw commands
            // cout << cnt << " 2asdasd" << endl;
            // D2.set_position(tx,-10,tz);
            if(checkpoint.size()<5)
            {
                rx = rand()%(50000);
                rz = rand()%(50000);
                if(rx>25000)
                    rx = rx-50000;
                if(rz>25000)
                    rz = rz-50000;

                Island temp = Island(rx/100,-16.6f,rz/100,COLOR_RED);
                checkpoint.push_back(temp);
            }
            draw();
            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);
            // tick_elements();
            tick_input(window);
            if(t1.processTick())
            {
                prevmouse_x = mouse_x;
                prevmouse_y = mouse_y;
                glfwGetCursorPos(window,&mouse_x,&mouse_y);
                move_x += mouse_x - prevmouse_x;
                move_y += mouse_y - prevmouse_y;
            }
        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();


    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
           (abs(a.y - b.y) * 2 < (a.height + b.height));
}

void reset_screen() {
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 6 / screen_zoom;
    float right  = screen_center_x + 6 / screen_zoom;
    // cout << left << " " << right << " " << top << " " << bottom << endl;
    // Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
    Matrices.projection = glm::perspective(left, right, bottom, top);
    // Matrices.projection = glm::perspective(45.0f, 1.0f, 60.0f, 1000.0f);

}
