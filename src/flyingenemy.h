#include "main.h"
#ifndef FLYINGENEMY_H
#define FLYINGENEMY_H


class Flyingenemy {
public:
    Flyingenemy() {}
    Flyingenemy(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation_z;
    float rotation_y;
    float rotation_x;
    bool exist;
    int n;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y, float z);
    void erase1();
    void tick(int move);
    void set_rotation();
    void set_speed(int move);
private:
    VAO *object;
    VAO *object1;
    VAO *object2;
};

#endif // FLYINGENEMY_H
