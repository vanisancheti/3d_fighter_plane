#include "main.h"

const color_t COLOR_RED =  		{ 236, 100, 75, 1};
const color_t COLOR_GREEN = 	{ 135, 211, 124,1};
const color_t COLOR_BLUE = 		{ 52,  73,  94, 1};
const color_t COLOR_LIGHTBLUE = { 0,   191, 255,1};
const color_t COLOR_BLACK = 	{ 0,   0,   0 , 1};
const color_t COLOR_BROWN = 	{ 102, 51,   0 , 1};
const color_t COLOR_GOLD = 		{ 249, 231, 71, 1};
const color_t COLOR_GREY = 		{ 96,  96,  96, 1};
const color_t COLOR_GREY_1 = 	{ 204, 204,255, 1};
const color_t COLOR_WGREY = 	{ 153, 255, 255,1};
const color_t COLOR_WALL = 		{ 192, 192, 192,1};
const color_t COLOR_VIOLET = 	{ 153,  51,  255,1};
const color_t COLOR_ORANGE = 	{ 255, 128, 0, 1};
const color_t COLOR_WHITE = 	{ 255, 255, 255 , 1};
const color_t COLOR_BACKGROUND= { 242, 241, 239,1};
