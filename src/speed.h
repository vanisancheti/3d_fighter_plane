#include "main.h"

#ifndef SPEED_H
#define SPEED_H


class Speed {
public:
    Speed() {}
    Speed(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation;
    float rotation_y;
    float rotation_z;
    void draw(glm::mat4 VP,glm::vec3 scale,int flag);
    void set_position(float x, float y, float z);
    void tick(int move);
    void set_rotation(int move);
    double speed;
private:
    VAO *object;
    VAO *object1;
    VAO *object2;
    VAO *object3;
    VAO *object4;
    VAO *object5;
};

#endif // SPEED_H
