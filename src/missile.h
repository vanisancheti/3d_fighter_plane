#include "main.h"

#ifndef MISSILE_H
#define MISSILE_H


class Missile {
public:
    Missile() {}
    Missile(float x, float y, float z, color_t color);
    glm::vec3 position;
    float rotation_z;
    float rotation_y;
    float rotation_x;
    bool exist;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y, float z);
    void tick(int move);
    void set_rotation(int move);
    void set_speed(int move);
    double speed_x,speed_y,speed_z;
    double acc_y;
    double acc_x;
    double acc_z;
private:
    VAO *object;
    VAO *object1;
};

#endif // MISSILE_H
