#include "parachute.h"
#include "main.h"
using namespace std;

Parachute::Parachute(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation_z = 0;
    this->rotation_x = 0;
    this->rotation_y = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A Island has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    int n=51;    
    GLfloat vertex_buffer_data1[2*n*3];
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    int temp=0;
    double z0=2.5f,y0=10.0f,x0=0.0f;
    double x1=0.0f,y1=10.0f,z1;
    double z2=0.0f,y2=10.0f,x2=0.0f;
    double x3=0.0f,z3=10.0f,y3;
    for(int i=0;i<n/3;i++)
    {
     
        y3 =y2*cos(-arg)-x2*sin(-arg);
        x3 =y2*sin(-arg)+x2*cos(-arg);
      
        y1 =y0*cos(-arg)-x0*sin(-arg);
        x1 =y0*sin(-arg)+x0*cos(-arg);

        vertex_buffer_data1[temp++] = x0;
        vertex_buffer_data1[temp++] = y0; 
        vertex_buffer_data1[temp++] = z0;

        vertex_buffer_data1[temp++] = x1;
        vertex_buffer_data1[temp++] = y1; 
        vertex_buffer_data1[temp++] = z0;

        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y2;
        vertex_buffer_data1[temp++] = z2;

        vertex_buffer_data1[temp++] = x3;
        vertex_buffer_data1[temp++] = y3; 
        vertex_buffer_data1[temp++] = z2;

        vertex_buffer_data1[temp++] = x1;
        vertex_buffer_data1[temp++] = y1;
        vertex_buffer_data1[temp++] = z0; 

        vertex_buffer_data1[temp++] = x2;
        vertex_buffer_data1[temp++] = y2; 
        vertex_buffer_data1[temp++] = z2;

        y0 = y1;
        x0 = x1;
        y2 = y3;
        x2 = x3;
    }

    // GLfloat vertex_buffer_data[2*n*9];
    // temp=0;
    // for(int i=0;i<2*n/3;i++)
    // {
     
    //     y3 =y2*cos(-arg)-x2*sin(-arg);
    //     x3 =y2*sin(-arg)+x2*cos(-arg);
      
    //     y1 =y0*cos(-arg)-x0*sin(-arg);
    //     x1 =y0*sin(-arg)+x0*cos(-arg);

    //     vertex_buffer_data[temp++] = x0-1.75f;
    //     vertex_buffer_data[temp++] = y0-3.5f; 
    //     vertex_buffer_data[temp++] = z0-8.0f;

    //     vertex_buffer_data[temp++] = x1-1.75f;
    //     vertex_buffer_data[temp++] = y1-3.5f; 
    //     vertex_buffer_data[temp++] = z0-8.0f;

    //     vertex_buffer_data[temp++] = x2-1.75f;
    //     vertex_buffer_data[temp++] = y2-3.5f;
    //     vertex_buffer_data[temp++] = z2-8.0f;

    //     vertex_buffer_data[temp++] = x3-1.75f;
    //     vertex_buffer_data[temp++] = y3-3.5f; 
    //     vertex_buffer_data[temp++] = z2-8.0f;

    //     vertex_buffer_data[temp++] = x1-1.75f;
    //     vertex_buffer_data[temp++] = y1-3.5f;
    //     vertex_buffer_data[temp++] = z0-8.0f; 

    //     vertex_buffer_data[temp++] = x2-1.75f;
    //     vertex_buffer_data[temp++] = y2-3.5f; 
    //     vertex_buffer_data[temp++] = z2-8.0f;

    //     y0 = y1;
    //     x0 = x1;
    //     y2 = y3;
    //     x2 = x3;
    // }
    
    this->object = create3DObject(GL_TRIANGLES,2*n, vertex_buffer_data1, color, GL_FILL);
    // this->object1 = create3DObject(GL_TRIANGLES,n*3, vertex_buffer_data1+n, color, GL_FILL);
    
}

void Parachute::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate_x    = glm::rotate((float) (this->rotation_x * M_PI / 180.0f), glm::vec3(1,0,0));
    glm::mat4 rotate_y    = glm::rotate((float) (this->rotation_y * M_PI / 180.0f), glm::vec3(0,1,0));
    glm::mat4 rotate_z    = glm::rotate((float) (this->rotation_z * M_PI / 180.0f), glm::vec3(0,0,1));
    glm::mat4 scale1 = glm::scale(scale);
    // No need as coords centered at 0, 0, 0 of Island arouund which we waant to rotate
    Matrices.model *= (translate*rotate_z*rotate_x*rotate_y);
    // this->rotation_y++;
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);   
    // draw3DObject(this->object1);   
}

void Parachute::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Parachute::tick(int move) {
     this->position.y-=0.1;
}
void Parachute::set_rotation() {
    // this->rotation_y += 1;
}
void Parachute::set_speed(int move) {

}