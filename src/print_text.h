#include "main.h"

#ifndef PRINT_TEXT_H
#define PRINT_TEXT_H


class Print_text {
public:
    Print_text() {}
    Print_text(float x, float y, float z,color_t color);
    glm::vec3 position;
    float rotation;
    bool exist;
    int number;
    int L_1;
    int L_2;
    int L_3;
    int L_4;
    int L_5;
    int L_6;
    int L_7;
    int L_8;
    int L_9;
    int L_10;
    int L_11;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    double x_speed;
private:
    VAO *object_1;
    VAO *object_2;
    VAO *object_3;
    VAO *object_4;
    VAO *object_5;
    VAO *object_6;
    VAO *object_7;
    VAO *object_8;
    VAO *object_9;
    VAO *object_10;
    VAO *object_11;

};

#endif // PRINT_TEXT_H
