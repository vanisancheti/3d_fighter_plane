#include "print_text.h"
#include "main.h"
    
using namespace std;
Print_text::Print_text(float x, float y, float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    x_speed = 0.05;
    this->exist = true;
    
    GLfloat vertex_buffer_data_1[]={
        -1.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        -1.0f,0.8f,0.0f,
        -1.0f,0.8f,0.0f,
        0.0f,0.8f,0.0f,
        0.0f,1.0f,0.0f
    };
    GLfloat vertex_buffer_data_2[]={
        -1.0f,0.2f,0.0f,
        0.0f,0.2f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        0.0f,0.0f,0.0f,
        0.0f,0.2f,0.0f
    };
    GLfloat vertex_buffer_data_3[]={
        -1.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        -1.0f,-0.8f,0.0f,
        -1.0f,-0.8f,0.0f,
        0.0f,-0.8f,0.0f,
        0.0f,-1.0f,0.0f
    };
    GLfloat vertex_buffer_data_4[]={
        -1.0f,1.0f,0.0f,
        -0.8f,1.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -0.8f,0.0f,0.0f,
        -0.8f,1.0f,0.0f
    };
    GLfloat vertex_buffer_data_5[]={
        -1.0f,-1.0f,0.0f,
        -0.8f,-1.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -0.8f,0.0f,0.0f,
        -0.8f,-1.0f,0.0f
    };    
    GLfloat vertex_buffer_data_6[]={
        0.0f,1.0f,0.0f,
        -0.2f,1.0f,0.0f,
        0.0f,0.0f,0.0f,
        0.0f,0.0f,0.0f,
        -0.2f,0.0f,0.0f,
        -0.2f,1.0f,0.0f
    };
    GLfloat vertex_buffer_data_7[]={
        0.0f,-1.0f,0.0f,
        -0.2f,-1.0f,0.0f,
        0.0f,0.0f,0.0f,
        0.0f,0.0f,0.0f,
        -0.2f,0.0f,0.0f,
        -0.2f,-1.0f,0.0f
    };  
    GLfloat vertex_buffer_data_8[]={
        -1.0f,0.0f,0.0f,
        -0.5f,-1.0f,0.0f,
        -0.8f,0.0f,0.0f,
        -0.8f,0.0f,0.0f,
        -0.5f,-1.0f,0.0f,
        -0.5f,-0.8f,0.0f
    };
    GLfloat vertex_buffer_data_9[]={
        -1.0f,0.0f,0.0f,
         0.0f,-1.0f,0.0f,
        -0.8f,0.0f,0.0f,
        -0.8f,0.0f,0.0f,
         0.0f,-1.0f,0.0f,
         0.0f,-0.8f,0.0f
    };
    GLfloat vertex_buffer_data_10[]={
        0.0f,0.0f,0.0f,
        -0.5f,-1.0f,0.0f,
        -0.2f,0.0f,0.0f,
        -0.2f,0.0f,0.0f,
        -0.5f,-1.0f,0.0f,
        -0.5f,-0.8f,0.0f
    };
    GLfloat vertex_buffer_data_11[]={
        0.0f,0.0f,0.0f,
         -1.0f,-1.0f,0.0f,
        -0.2f,0.0f,0.0f,
        -0.2f,0.0f,0.0f,
         -1.0f,-1.0f,0.0f,
         -1.0f,-0.8f,0.0f  
    };
    this->object_1 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_1, color, GL_FILL);
    this->object_2 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_2, color, GL_FILL);
    this->object_3 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_3, color, GL_FILL);
    this->object_4 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_4, color, GL_FILL);
    this->object_5 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_5, color, GL_FILL);
    this->object_6 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_6, color, GL_FILL);
    this->object_7 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_7, color, GL_FILL);
   
}

void Print_text::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    scale = glm::vec3(0.01,0.01,0.01);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    if(this->L_1 == 1)
        draw3DObject(this->object_1);
    if(this->L_2 == 1)
        draw3DObject(this->object_2);
    if(this->L_3 == 1)
        draw3DObject(this->object_3);
    if(this->L_4 == 1)
        draw3DObject(this->object_4);
    if(this->L_5 == 1)
        draw3DObject(this->object_5);
    if(this->L_6 == 1)
        draw3DObject(this->object_6);
    if(this->L_7 == 1)
        draw3DObject(this->object_7);
    
}

void Print_text::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Print_text::tick(int move) {
   
}


