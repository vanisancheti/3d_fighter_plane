#include "main.h"
#include "timer.h"
#include "ground.h"
#include "cube.h"
#include "island.h"
#include "arrow.h"
#include "dashboard.h"
#include "cannon.h"
#include "bombs.h"
#include "smoke.h"
#include "volcano.h"
#include "missile.h"
#include "parachute.h"
#include "flyingenemy.h"
#include "print_text.h"
#include "fuel.h"
#include "compass.h"


using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Cube ball1;
Compass C1;
Ground sea;
Island I1;
Arrow A1;
Missile M1;
Dashboard D1;
Parachute P1;
Flyingenemy F1;
Fuel Fuel1;

vector<Island> checkpoint;
vector<Cannon> Cannons;
vector<Smoke> smokerings;
vector<Bombs> Fire; 
vector<Bombs> Fire1; 
vector<Bombs> Fire2;
vector<Volcano> volcanos;
vector<Missile> missiles;
vector<Flyingenemy> flyingenemies;
vector<Print_text> score_print;

vector<Volcano>::iterator vit;
vector<Bombs>::iterator Bit;
vector<Island>::iterator Iit;
vector<Cannon>::iterator Cit;
vector<Missile>::iterator Mit;
vector<Smoke>::iterator Sit;
vector<Flyingenemy>::iterator Fit;
vector<Print_text>:: iterator s_it;

double score_pos = 0.0f;
double curr_time=0;
double prev_time=0;
int View;
int cnt=0;
int flycnt=0;
int ff=0;
int rx,ry,rz;
double mouse_x,mouse_y;
double prevmouse_x,prevmouse_y;
double move_x=0,move_y=0;

// View = 0 Plane View
// View = 1 Top View
// View = 2 Tower View
// View = 3 Follow-cam View
// View = 4 Helicopter-cam View 


float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
Timer t60(1.0 / 60);
Timer t1(1.0 / 30);
Timer t5(1.0/2.0);
Timer t2b(1/1);
Timer t21b(1/1);
Timer t5b(5/1);
double cx,cy,cz,tx,ty,tz;
/* Render the scene with openGL */
/* Edit this function according to your assignment */
double get_camera(int x)
{
    if(x==0)
        return cx;
    if(x==1)
        return cy;
    if(x==2)
        return cz;
    if(x==3)
        return tx;
    if(x==4)
        return ty;
    if(x==5)
        return tz;
}
Print_text fix_pos(int x,Print_text temp)
{
    temp.L_1=0,temp.L_2=0,temp.L_3=0,temp.L_4=0,temp.L_5=0,temp.L_6=0,temp.L_7=0,temp.L_8=0,temp.L_9=0,temp.L_10=0,temp.L_11=0;
    if(x==0)
        temp.L_1 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_6 =1,temp.L_7 =1;
    else if(x==1)
        temp.L_6 =1,temp.L_7 =1;
    else if(x==2)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_5 =1,temp.L_6 =1;
    else if(x==3)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_6 =1,temp.L_7 =1;
    else if(x==4)
        temp.L_2 =1,temp.L_4 =1,temp.L_6 =1,temp.L_7 =1;
    else if(x==5)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_7 =1;
    else if(x==6)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_7 =1;
    else if(x==7)
        temp.L_1 =1,temp.L_6 =1,temp.L_7 =1;
    else if(x==8)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_6 =1,temp.L_7 =1;
    else if(x==9)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_6 =1,temp.L_7 =1;   
    return temp;
}
void print_score()
{
    score_print.clear();
    // vector<Print_text>::iterator it;
    // it = score_print.begin();
    // while(score_print.size()!=0)
    // {
    //     score_print.erase(it);
    // }
    int temp = 1000;
    while(temp>1 && ball1.score%temp != 0 && ball1.score/temp == 0)
        temp = temp/10;

    // cout << temp << endl;
    if(ball1.score == 0)
        temp =10;
    
    score_pos=0.0f;
    double x = (ball1.score/temp);
    // int y = ball1.score;
    while(temp!=0)
    {
        Print_text t = Print_text(-3+score_pos,-12.0f,-40.0f,COLOR_WHITE);
        score_pos+=1.5f;
        t = fix_pos(x,t);
        t.number = x;
        score_print.push_back(t);

        x = ball1.score%temp;
        temp = temp/10;
        if(temp!=0)
        x = int(x/temp);
        // cout << temp  << " " << score_print.size() <<" " <<  x  << " " << ball1.score << endl;
    }
    return ;
}
void detect_collision_bombs()
{
    Bit = Fire.begin();
    while(Bit!=Fire.end())
    {
        if(abs(Bit->position.x - ball1.position.x) < 1 && abs(Bit->position.z - ball1.position.z)<1 && abs(Bit->position.y - ball1.position.y)<1)
        {
            Bit->exist = false;
            Fire.erase(Bit);
            if(ball1.score >0)
            {
                ball1.score--;
                print_score();
            }
            Bit--;
        }
        Bit++;
    }
    Bit = Fire1.begin();
    while(Bit!=Fire1.end())
    {
        if(abs(Bit->position.x - ball1.position.x) < 1 && abs(Bit->position.z - ball1.position.z)<1 && abs(Bit->position.y - ball1.position.y)<1)
        {
            Bit->exist = false;
            Fire1.erase(Bit);
            if(ball1.score >0)
            {
                ball1.score--;
                print_score();
            }
            Bit--;
        }
        Bit++;
    }
    Bit = Fire2.begin();
    while(Bit!=Fire2.end())
    {
        Cit = Cannons.begin();
        while(Cit!=Cannons.end())
        {
            if(abs(Bit->position.x - Cit->position.x) < 5 && abs(Bit->position.z - Cit->position.z)<5 && abs(Bit->position.y - Cit->position.y)<5)
            {
                ball1.score+=5;
                print_score();
                Cannons.erase(Cit);
                Cit--;
            }
            Cit++;
        }
        Bit++;
    }
}
void detect_collision_smoke_ring()
{
    Sit = smokerings.begin();
    while(Sit!=smokerings.end())
    {
        if(Sit->exist && abs(ball1.position.x - (Sit->position.x+1*cos(Sit->rotation_y*180/M_PI))<30 && abs(ball1.position.z - (Sit->position.z-1*sin(Sit->rotation_y*180/M_PI)))<10))
        {
            ball1.score+=10;
            print_score();
            Sit->exist = false;
        }
        Sit++;
    }

    if(Fuel1.exist && abs(ball1.position.x - Fuel1.position.x)<5 && abs(ball1.position.y-Fuel1.position.y)<5 && abs(ball1.position.z-Fuel1.position.z)<5)
    {
        Fuel1.exist = false;
        ball1.fuel = 100;
        D1.rotation_y2 = -90;
    }
}
void detect_collision_volcano()
{
    vit = volcanos.begin();
    while(vit!=volcanos.end())
    {
        if(abs(ball1.position.x - vit->position.x) < 10 && abs(ball1.position.z - vit->position.z) < 10)
        {
            // cout << "No movement Zone" << endl;
            // if(ball1.score>0)
            ball1.score=0;
            print_score();
        }   
        vit++;
    }
}
void detect_collision_checkpoint()
{
    Mit = missiles.begin();
    while(Mit!=missiles.end())
    {
        Cit = Cannons.begin();
        while(Cit!=Cannons.end())
        {
            if(abs(Cit->position.x - Mit->position.x)<5 && abs(Cit->position.y - Mit->position.y)<5 && abs(Cit->position.z - Mit->position.z)<5)
            {
                // cout << "detect_collision" << endl;
                ball1.score+=5;
                print_score();
                Cannons.erase(Cit);
                Cit--;
            }
            Cit++;
        }
        Mit++;
    }
    Mit = missiles.begin();
    while(Mit!=missiles.end())
    {
        Fit = flyingenemies.begin();
        while(Fit!=flyingenemies.end())
        {
            if(abs(Fit->position.x - Mit->position.x)<5 && abs(Fit->position.y - Mit->position.y)<10 && abs(Fit->position.z - Mit->position.z)<5)
            {
                // cout << "detect_collision" << endl;
                ball1.score+=15;
                print_score();
                Fit->exist = false;
            }
            Fit++;
        }
        Mit++;
    }

}
void change(double a,double b,double c,double d,double e,double f)
{
    cx = a,cy=b,cz=c,tx=d,ty=e,tz=f;
}
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    // glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 0, 5*sin(camera_rotation_angle*M_PI/180.0f) );
    glm::vec3 eye;

    if(View==0)
    {
        cx=ball1.position.x-10*cos(ball1.rotation_y * M_PI / 180.0f);
        cy=ball1.position.y+30;
        cz=ball1.position.z+10*sin(ball1.rotation_y * M_PI / 180.0f);
    }
        // eye = glm::vec3(ball1.position.x,ball1.position.y+20,ball1.position.z);
    if(View==1)
         cx=ball1.position.x,cy=ball1.position.y+400,cz=ball1.position.z;
        // eye = glm::vec3( ball1.position.x,100, ball1.position.z);
    if(View==2)
        // cx =800,cy=10,cz=800;
        cx =500,cy=200,cz=500;
    int rad = 100;
    if(View==3)
    {
        cx=ball1.position.x-100*cos(ball1.rotation_y * M_PI / 180.0f);;
        cy=ball1.position.y+20;
        cz=ball1.position.z+100*sin(ball1.rotation_y * M_PI / 180.0f);   
    }
    if(View==4)
    {
        cz=ball1.position.z+rad*cos((10*move_x/rad)*M_PI/180.0f);
        cx=ball1.position.x+rad*sin((10*move_x/rad)*M_PI/180.0f);
        cy=ball1.position.y+(0.2)*move_y;
    }
    eye = glm::vec3(cx,cy,cz);

    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    
    glm::vec3 target;
    
    
    if(View==0)
        tx=ball1.position.x+5*cos(ball1.rotation_y * M_PI / 180.0f),ty=ball1.position.y+20,tz=ball1.position.z-5*sin(ball1.rotation_y * M_PI / 180.0f);
    if(View==1)
        tx=ball1.position.x+0.1,ty=ball1.position.y,tz=ball1.position.z;
    if(View==2)
        tx=ball1.position.x,ty=0,tz=ball1.position.z;
    if(View==3)
        tx=ball1.position.x,ty=ball1.position.y,tz=ball1.position.z;
    if(View==4)
        tx=ball1.position.x,ty=ball1.position.y,tz=ball1.position.z;

    target = glm::vec3(tx,ty,tz);

    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up;
    up=glm::vec3(0,1,0);
    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane
    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;
    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    // Scene render
    glm::vec3 scale = glm::vec3(1.0f,1.0f,1.0f);


    // cout << checkpoint.size() << " checkpoint" << endl;
    // cout << Cannons.size() << " cannons" << endl;
    // cout << Fire.size()  << " Fire" << endl;
    // cout << Fire1.size() << " Fire1" << endl;
    // cout << volcanos.size() << " volcanos" << endl;
    // cout << missiles.size() << " missile" << endl;
    // cout << flyingenemies.size() << " flyingenemies" << endl;
    // cout << score_print.size() << " score_print" << endl;

    ball1.draw(VP,scale);
    D1.draw(Matrices.projection,scale);
    sea.draw(VP,scale);
    M1.draw(VP,scale);
    C1.draw(Matrices.projection,scale);
    if(Fuel1.exist)
    Fuel1.draw(VP,scale);
    s_it = score_print.begin();
    while(s_it!=score_print.end())
    {
        // cout << s_it->number << endl;
        s_it->draw(Matrices.projection,scale);
        s_it++;
    }
    // P1.draw(VP,scale);
    // if(F1.exist)
    // F1.draw(VP,scale);
    Fit = flyingenemies.begin();
    int x=0;
    while(Fit!=flyingenemies.end())
    {
        Fit->n = x*20;
        if(Fit->exist)
            Fit->draw(VP,scale);
        else
        {
            Fit->erase1();
            flyingenemies.erase(Fit);
            flycnt-=20;
            continue;
        }
        x++;
        // cout << Fit->n << endl;
        Fit++;
    }
    // V1.draw(VP,scale);
    A1.draw(VP,scale);
    vit = volcanos.begin();
    while(vit!=volcanos.end())
    {
        vit->draw(VP,scale);
        vit++;
    }
    Sit = smokerings.begin();
    while(Sit!=smokerings.end())
    {
        if(Sit->exist)
            Sit->draw(VP,scale);
        Sit++;
    }
    Mit = missiles.begin();
    while(Mit!=missiles.end())
    {
        if(Mit->exist)
        {
            Mit->draw(VP,scale);
        }
        else
        {
            missiles.erase(Mit);
            Mit--;
        }
        Mit++;
    }
    Iit = checkpoint.begin();
    while(Iit!=checkpoint.end())
    {
        (*Iit).draw(VP,scale);
        Iit++;
    }
    Cit = Cannons.begin();
    while(Cit!=Cannons.end())
    {
        (*Cit).draw(VP,scale);
        Cit++;
    }
    Bit = Fire.begin();
    while(Bit!=Fire.end())
    {
        if(Bit->exist)
        Bit->draw(VP,scale);
        else
        {
            Fire.erase(Bit);
            Bit--;
        }
        Bit++;
    }

     Bit = Fire1.begin();
    while(Bit!=Fire1.end())
    {
        if(Bit->exist)
        Bit->draw(VP,scale);
        else
        {
            Fire1.erase(Bit);
            Bit--;
        }
        Bit++;
    }    // scale = glm::vec3(10.0f,10.0f,10.0f);

    Bit = Fire2.begin();
    while(Bit!=Fire2.end())
    {
        if(Bit->exist)
        Bit->draw(VP,scale);
        else
        {
            Fire2.erase(Bit);
            Bit--;
        }
        Bit++;
    } 
}
void release_missile()
{
    if(t2b.processTick())
    {
        Missile temp;
        temp = Missile(ball1.position.x,ball1.position.y-5,ball1.position.z,COLOR_BLACK);
        temp.exist = true;                  
        temp.speed_y = 0;
        temp.rotation_y = ball1.rotation_y;
        temp.acc_x = 30*(cos(ball1.rotation_y * M_PI / 180.0f));
        temp.acc_z = -30*(sin(ball1.rotation_y * M_PI / 180.0f));
        missiles.push_back(temp);
    }
}
void release_bombs()
{
    if(t21b.processTick())
    {
        Bombs temp;
        temp = Bombs(ball1.position.x,ball1.position.y-5,ball1.position.z,COLOR_BLACK);
        temp.exist = true;                  
        temp.speed_y = 0;
        Fire2.push_back(temp);
    } 
}
void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int up = glfwGetKey(window, GLFW_KEY_UP);
    int down = glfwGetKey(window, GLFW_KEY_DOWN);
    int spacebar = glfwGetKey(window, GLFW_KEY_SPACE);
    int V = glfwGetKey(window, GLFW_KEY_V);
    //tilt left and right
    int A = glfwGetKey(window, GLFW_KEY_A);
    int D = glfwGetKey(window, GLFW_KEY_D);
    //rotate clockwise and anticlockwise
    int Q = glfwGetKey(window, GLFW_KEY_Q);
    int E = glfwGetKey(window, GLFW_KEY_E);
    int W = glfwGetKey(window, GLFW_KEY_W);
    int S = glfwGetKey(window, GLFW_KEY_S);



    if(V)
        cnt = (cnt+1)%25;
    if(cnt<25)
        View = 4;
    if(cnt<20)
        View = 3;
    if(cnt<15)
        View = 2;
    if(cnt<10)
        View = 1;
    if(cnt<5)
        View = 0;

    if(spacebar)
    {
        ball1.tick(3);
        D1.set_rotation(6,1.8*ball1.speed/1.5);
    }
    if(S)
    {
        ball1.tick(1);
        D1.set_rotation(5,1.8*ball1.speed/1.5);
    }

    if(A)
    {
        ball1.set_rotation_x(0);
    }
    if(D)
    {
        ball1.set_rotation_x(1);
    }
    if(Q)
    {
        ball1.set_rotation_y(0);

    }
    if(E)
    {
        ball1.set_rotation_y(1);
   
    }
    if(down)
    {
        ball1.tick(0);
        cy++;
    }
    if(up)
    {
        ball1.tick(2);
        cy--;
    }
    if(W)
    {
        if(ball1.speed < 18)
        {
            ball1.speed+=0.1;
            ball1.tick(2);
            D1.set_rotation(2,0);
        }
    }
    else
    {
        if(ball1.speed > 0.5)
        {
            ball1.speed-=0.1;
            ball1.tick(2);
            D1.set_rotation(1,0);
        }
    }
}

void tick_elements() {
    // ball1.tick();
    // camera_rotation_angle += 1;
    Sit = smokerings.begin();
    while(Sit!=smokerings.end())
    {
        Sit->set_rotation();
        Sit++;
    }
    M1.set_position(ball1.position.x+6*cos(ball1.rotation_y * M_PI / 180.0f),ball1.position.y-5,ball1.position.z-6*sin(ball1.rotation_y * M_PI / 180.0f));
    M1.rotation_y=ball1.rotation_y;
    C1.rotation_y = -ball1.rotation_y;

    //1 to denote decreased speed
    //2 to denote increased speed
    //3 to denote decreased fuel
    //4 to denote increased fuel
    // D1.set_rotation(4);
    // F1.tick(0);
  

    Fit = flyingenemies.begin();
    while(Fit!=flyingenemies.end())
    {
        if(Fit->position.y < -50)
            Fit->exist=false;
        Fit->tick(0);
        Bombs temp;
        if(abs(ball1.position.z - Fit->position.z)<50 && abs(ball1.position.x - Fit->position.x) <50 && t5.processTick())
        {
            temp = Bombs(Fit->position.x,Fit->position.y-10,Fit->position.z,COLOR_ORANGE);
            temp.exist = true;                  
            temp.speed_y = (ball1.position.y - temp.position.y)/2 + 9.8;
            temp.acc_x = 0.5*(ball1.position.x - Fit->position.x);
            temp.acc_z = 0.5*(ball1.position.z - Fit->position.z);
            Fire1.push_back(temp);
        }
        Fit++;
    }
    // if(F1.position.y < -50)
        // F1.exist=false;
    D1.set_rotation(3,0);
    ball1.fuel-=0.018;
    Mit = missiles.begin();
    while(Mit!=missiles.end())
    {
        Mit->tick(0);
        Mit++;
    }
    Cit = Cannons.begin();
    if(Cannons.size()!=0)
    {
        A1.set_position(tx+50*cos(ball1.rotation_y * M_PI / 180.0f),ty-15,tz-50*sin(ball1.rotation_y * M_PI / 180.0f));
        if(Cit->position.x < A1.position.x)            
            A1.rotation_y = -(atan((Cit->position.z-A1.position.z+1)/(Cit->position.x-A1.position.x+1))*180/M_PI);
        else
            A1.rotation_y = 180-(atan((Cit->position.z-A1.position.z+1)/(Cit->position.x-A1.position.x+1))*180/M_PI);
    }
    else
    {
        ball1.score=0;
    }


    Cit = Cannons.begin();
    Bombs temp;
    while(Cit!=Cannons.end())
    {
        
        if(ball1.position.x < Cit->position.x)
        Cit->rotation_y = 180-(atan((ball1.position.z - Cit->position.z)/(ball1.position.x - Cit->position.x))*180/M_PI);
        else
        Cit->rotation_y = -(atan((ball1.position.z - Cit->position.z)/(ball1.position.x - Cit->position.x))*180/M_PI);

        if(Cit->rotation_y > 360)
            Cit->rotation_y-=360;
        // if(ball1.position.z > Cit->position.z)
        // Cit->rotation_x = 180-(atan((ball1.position.z- Cit->position.z)/(ball1.position.y - Cit->position.y))*180/M_PI);
        // else
        // Cit->rotation_x = -(atan((ball1.position.z - Cit->position.z)/(ball1.position.y - Cit->position.y))*180/M_PI);
        
        if(abs(ball1.position.z - Cit->position.z)<50 && abs(ball1.position.x - Cit->position.x) <50 && t5.processTick())
        {
            temp = Bombs(Cit->position.x,Cit->position.y,Cit->position.z,COLOR_ORANGE);
            temp.exist = true;                  
            temp.speed_y = (ball1.position.y - temp.position.y)/2 + 9.8;
            temp.acc_x = 0.5*(ball1.position.x - Cit->position.x);
            temp.acc_z = 0.5*(ball1.position.z - Cit->position.z);
            Fire.push_back(temp);
        }
        Cit++;
    }
    Bit = Fire.begin();
    while(Bit!=Fire.end())
    {
        if(Bit->exist)
        Bit->tick(0);
        else
        {
            Fire.erase(Bit);
            Bit--;
        }
        Bit++;
    }
    Bit = Fire1.begin();
    while(Bit!=Fire1.end())
    {
        if(Bit->exist)
        Bit->tick(0);
        else
        {
            Fire1.erase(Bit);
            Bit--;
        }
        Bit++;
    }
    Bit = Fire2.begin();
    while(Bit!=Fire2.end())
    {
        if(Bit->exist)
            Bit->tick(0);
        else
        {
            Fire2.erase(Bit);
            Bit--;
        }
        Bit++;
    }
}
double curr() {
    return curr_time;
}
double prev() {
    return prev_time;
}
/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models

    ball1 = Cube(0,10,0, COLOR_GREEN);
    sea = Ground(0,-50,0,COLOR_BLUE);
    D1 = Dashboard(0,1,0,COLOR_BLACK);
    A1 = Arrow(0,-30,0,COLOR_BLACK);
    M1 = Missile(ball1.position.x,ball1.position.y-1,ball1.position.z+6,COLOR_BLACK);
    // P1 = Parachute(0,0,0,COLOR_GOLD);
    C1 = Compass(-0.75,+1.0f,-3.0f,COLOR_WHITE);
    rx = rand()%50000;
    ry = rand()%100;
    rz = rand()%50000;
    if(rx>25000)
        rx = rx-50000;
    if(rz>25000)
        rz = rz-50000;

    Fuel1 = Fuel(rx/100,ry,rz/100,COLOR_VIOLET);   
    Fuel1.exist = false;
    // rx = rand()%50000;
    // ry = rand()%1000;
    // rz = rand()%50000;
    // if(rx>25000)
    //     rx = rx-50000;
    // if(rz>25000)
    //     rz = rz-50000;
    // F1 = Flyingenemy(rx/100,ry/10,rz/100,COLOR_GOLD);
    // F1.exist=true;
    
    
    
    // cout << F1.position.y << " " << F2.position.y << " " << F3.position.y << endl;
    // V1 = Volcano(0,-40.0f,0,COLOR_BROWN);


    while(volcanos.size()<3)
    {
        rx = rand()%(50000);
        rz = rand()%(50000);
        if(rx>25000)
            rx = rx-50000;
        if(rz>25000)
            rz = rz-50000;
        Volcano temp;
        temp = Volcano(rx/100,-40.0f,rz/100,COLOR_BROWN);
        volcanos.push_back(temp);   
    }
    while(checkpoint.size()<10)
    {
        rx = rand()%(50000);
        rz = rand()%(50000);
        if(rx>25000)
            rx = rx-50000;
        if(rz>25000)
            rz = rz-50000;
        Island temp;
        Cannon temp1;

        if(checkpoint.empty())
        temp = Island(rx/100,-50,rz/100,COLOR_GOLD), temp1 = Cannon(rx/100,-27,rz/100,COLOR_GOLD);
        else
        temp = Island(rx/100,-50,rz/100,COLOR_RED), temp1 = Cannon(rx/100,-27,rz/100,COLOR_RED);

        checkpoint.push_back(temp);
        Cannons.push_back(temp1);
    }
    while(smokerings.size()<5)
    {
        rx = rand()%(50000);
        rz = rand()%(50000);
        if(rx>25000)
            rx = rx-50000;
        if(rz>25000)
            rz = rz-50000;
        Smoke temp;
        temp = Smoke(rx/100,10,rz/100,COLOR_GREY_1);
        temp.exist = true;
        smokerings.push_back(temp);
    }
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    

    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 1.0f); // R, G, B, A
    glClearDepth (1.0f);
    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 1600;
    int height = 1600;

    window = initGLFW(width, height);

    initGL (window, width, height);
    glfwGetCursorPos(window,&prevmouse_x,&prevmouse_y);
    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
             curr_time = glfwGetTime();
            // 60 fps
            // OpenGL Draw commands
             // cout << ball1.score << endl;
            if(ball1.score<=0 || ball1.position.y <= -50)
            {
                cout << "SCORE :: " << ball1.score << endl;
                quit(window);
                break;
            }
            rx = rand()%50000;
            ry = rand()%100;
            rz = rand()%50000;
            if(rx>25000)
                rx = rx-50000;
            if(rz>25000)
                rz = rz-50000;
            // cout << ball1.fuel << endl;
            if(!Fuel1.exist && ball1.fuel<75)
            { 
                Fuel1.set_position(rx/100,ry,rz/100);
                // cout << Fuel1.position.x << " " << Fuel1.position.y << " " << Fuel1.position.z << endl;
                Fuel1.exist = true;
            }
            Flyingenemy temp;
            while(flyingenemies.size()<3 && t5b.processTick())
            {
                rx = rand()%50000;
                ry = rand()%3000;
                rz = rand()%50000;
                if(rx>25000)
                    rx = rx-50000;
                if(rz>25000)
                    rz = rz-50000;
                temp = Flyingenemy(rx/100,ry/10,rz/100,COLOR_GOLD);
                temp.exist=true;
                temp.n=flycnt;
                flyingenemies.push_back(temp);
                flycnt+=20;
                // cout << flycnt << endl;
            }
            
            detect_collision_bombs();
            detect_collision_volcano();
            detect_collision_checkpoint();
            detect_collision_smoke_ring();
            draw();
            // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);
            reset_screen();
            tick_elements();
            tick_input(window);
            if(t1.processTick())
            {
                prevmouse_x = mouse_x;
                prevmouse_y = mouse_y;
                glfwGetCursorPos(window,&mouse_x,&mouse_y);
                move_x += mouse_x - prevmouse_x;
                move_y += mouse_y - prevmouse_y;
                // cout << move_x << " " << move_y << endl;
            }
            prev_time = curr_time;
        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();


    }
    cout << "SCORE :: " << ball1.score << endl;
    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
           (abs(a.y - b.y) * 2 < (a.height + b.height));
}

void reset_screen() {
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 6 / screen_zoom;
    float right  = screen_center_x + 6 / screen_zoom;
    // cout << left << " " << right << " " << top << " " << bottom << endl;
    // Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
    // Matrices.projection = glm::perspective(left, right, bottom, top);
    Matrices.projection = glm::perspective(glm::radians(45.0f/screen_zoom), 1.0f, 0.1f/screen_zoom, 1000.0f/screen_zoom);

}
